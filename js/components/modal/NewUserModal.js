(function (window) {

    class NewUserModal extends window.AbstractModal {

        constructor(gotoWelcomeViewCallback) {
            super("new-user-modal", {
                $domElement: $("#new-user-modal")
            });

            this.gotoWelcomeViewCallback = gotoWelcomeViewCallback;

            this.$form = this.$domElement.find("#new-user-form");
            this.$confirmBtn = this.$domElement.find("#new-user-modal__confirm-btn");
            this.$brandSelect = this.$form.find("#new-user-form__brands-select");

            this.$firstname = this.$form.find('[name="first_name"]');
            this.$lastname = this.$form.find('[name="last_name"]');
            this.$email = this.$form.find('[name="email"]');
            this.$validator;
        }

        init() {
            let validatorRules = {
                'first_name': {
                    required: true,
                    lettersonly: true
                },
                'last_name': {
                    required: true,
                    lettersonly: true
                },
                'email': {
                    required: true,
                    email: true
                },
                'brands': {
                    required: true
                }
            };

            let validatorMessages = {
                'first_name': {
                    lettersonly: this.$firstname.attr('data-lettersonly')
                },
                'last_name': {
                    lettersonly: this.$lastname.attr('data-lettersonly')
                }
            };

            let _this = this;

            this.$validator = this.$form.validate({
                rules: validatorRules,
                messages: validatorMessages,
                errorElement: 'small',
                errorClass: 'form-text text-muted',
                highlight: function (element) {
                    // console.log(element);

                    $(element).closest('.form-group').addClass('form-error');
                },
                unhighlight: function (element) {

                    $(element).closest('.form-group').removeClass('form-error');
                },
                submitHandler: function (form) {
                    try {
                        _this.gotoWelcomeViewCallback({
                            new_user: true,
                            user_id: (Globals.isRealDeviceApp && window.device ? device.uuid : window.makeID(10)) + "_" + parseInt(window.Date.now() * Math.random()),
                            last_name: _this.$form.find("#new-user-form__last-name").val(),
                            first_name: _this.$form.find("#new-user-form__first-name").val(),
                            email: _this.$form.find("#new-user-form__email").val(),
                            vehicle_brand: _this.$form.find("#new-user-form__brands-select").val()
                        });
                        _this.reset();
                        _this.hide();
                    } catch (e) {
                        console.log('form-error');
                        console.log(e);
                    }
                }
            });

            super.init();
            this.update();
        }

        modalShowHandler(event = null) {
            this.$form.find("#new-user-form__last-name").val("");
            this.$form.find("#new-user-form__first-name").val("");
            this.$form.find("#new-user-form__email").val("");
            this.$brandSelect.val("").html("");
            this.$validator.resetForm();

            let markup = '<option disabled selected value="---"> -- Select an option -- </option>';

            for (let i = 0, len = window.AppStaticData.BRANDS_LIST.length; i < len; i++) {
                markup += '<option value="' + window.AppStaticData.BRANDS_LIST[i].name + '">' + window.AppStaticData.BRANDS_LIST[i].name + '</option>';
            }

            this.$brandSelect.html(markup);
            this.update();
        }

        modalHiddenHandler(event = null) {
            this.$brandSelect.off("change");
        }

        reset() {
            this.$form[0].reset();
        }

        dispose() {
            this.$brandSelect = null;

            this.$form.off();
            this.$form = null;

            this.$confirmBtn.off();
            this.$confirmBtn = null;

            super.dispose();
        }
    }

    window.NewUserModal = NewUserModal;
})(window);
