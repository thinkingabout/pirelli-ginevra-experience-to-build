(function (window) {

    class ListUpdateModal extends window.AbstractModal {

        constructor() {
            const OPTIONS = {
                $domElement: $("#list-update-modal"),
                modal: {
                    show: false,
                    focus: false,
                    keyboard: false,
                    backdrop: "static"
                }
            };

            super("list-update-modal", OPTIONS);
        }
    }

    window.ListUpdateModal = ListUpdateModal;
})(window);
