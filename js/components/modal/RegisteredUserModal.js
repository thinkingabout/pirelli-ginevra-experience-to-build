(function (window) {

    class RegisteredUserModal extends window.AbstractModal {

        constructor(gotoWelcomeViewCallback) {
            super("registered-user-modal", {
                $domElement: $("#registered-user-modal")
            });

            this.currentUser = null;
            this.gotoWelcomeViewCallback = gotoWelcomeViewCallback;
            this.$username = this.$domElement.find("#registered-user-modal__username");
            this.$confirmBtn = this.$domElement.find("#registered-user-modal__confirm-btn");
        }

        init() {
            super.init();

            this.$confirmBtn.on("click", event => {
                event.preventDefault();
                this.gotoWelcomeViewCallback(this.currentUser);
                this.currentUser = null;
            });
        }

        show(data) {
            this.currentUser = data;
            this.currentUser.experienceDone ? this.$domElement.addClass("experience-done") : this.$domElement.removeClass("experience-done");
            this.$username.html(this.currentUser.last_name + " " + this.currentUser.first_name);
            this.update();

            super.show();
        }

        dispose() {
            this.currentUser = null;
            this.$confirmBtn.off("click");
            this.$confirmBtn = null;
            this.$username = null;
            super.dispose();
        }
    }

    window.RegisteredUserModal = RegisteredUserModal;
})(window);
