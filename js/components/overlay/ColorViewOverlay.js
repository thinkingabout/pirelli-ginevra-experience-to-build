(function (window) {

    class ColorViewOverlay extends window.AbstractViewOverlay {

        constructor(id, options) {

            super(id, options);

            this.$content = this.$domElement.find(".view-overlay-content");
        }

        show(animated = true) {

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.fromTo(this.$content, 0.65, {
                xPercent: 120
            }, {
                force3D: "auto",
                xPercent: 0,
                ease: window.Expo.easeOut
            });

            super.show(animated, nestedTL);
        }

        hide(animated = true) {
            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.to(this.$content, 0.65, {
                force3D: "auto",
                xPercent: 110,
                ease: window.Expo.easeInOut
            });

            super.hide(animated, nestedTL);
        }

        dispose() {
            this.$content = null;

            super.dispose();
        }
    }

    window.ColorViewOverlay = ColorViewOverlay;
})(window);
