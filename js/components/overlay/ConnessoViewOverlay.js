(function (window) {

    class ConnessoViewOverlay extends window.AbstractViewOverlay {

        constructor(id, options) {

            super(id, options);

            this.$backdrop = this.$domElement.find(".view-overlay-backdrop");
            this.$content = this.$domElement.find(".view-overlay-content");
            this.$video = this.$content.find("#connesso-view-overlay__video-wrapper video");
            this.$closeBtn = this.$content.find(".btn");
            this.$progressBar = this.$content.find("#connesso-view-overlay__video-wrapper div span");
        }

        init() {
            this.$closeBtn.on("click", event => {
                this.hide();
            });

            this.$video.on("click", event => {
                this.$video.get(0).paused ? this.$video.get(0).play() : this.$video.get(0).pause();
            });

            this.$video.on("timeupdate", () => {
                const CURRENT_TIME = this.$video.get(0).currentTime;
                const MAX_DURATION = this.$video.get(0).duration;
                this.$progressBar.css("width", (100 * CURRENT_TIME / MAX_DURATION).toString() + "%");
            });

            this.$video.one("ended", () => {
                this.$closeBtn.trigger("click");
            });

            super.init();
        }

        show(animated = true) {
            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.fromTo(this.$backdrop, 0.75, { opacity: 0 }, { force3D: "auto", opacity: 1, ease: window.Cubic.easeOut });
            nestedTL.fromTo(this.$content, 0.65, { opacity: 0, scale: 0.9 }, { force3D: "auto", transformOrigin: "center", opacity: 1, scale: 1, ease: window.Expo.easeOut }, 0.3);

            super.show(animated, nestedTL);
        }

        hide(animated = true) {
            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.to(this.$content, 0.65, { force3D: "auto", opacity: 0, scale: 0.9, ease: window.Expo.easeInOut });
            nestedTL.to(this.$backdrop, 0.65, { force3D: "auto", opacity: 0, ease: window.Cubic.easeOut }, 0.3);

            super.hide(animated, nestedTL);
        }

        onShowStart() {
            this.$progressBar.css("width", 0);
            this.$video.get(0).currentTime = 0;
            this.$video.get(0).play();
            super.onShowStart();
        }

        onHideStart() {
            this.$video.get(0).pause();
            super.onHideStart();
        }

        dispose() {
            this.$closeBtn.off("click");
            this.$closeBtn = null;
            this.$video.off();
            this.$video = null;
            this.$backdrop = null;
            this.$content = null;

            super.dispose();
        }
    }

    window.ConnessoViewOverlay = ConnessoViewOverlay;
})(window);
