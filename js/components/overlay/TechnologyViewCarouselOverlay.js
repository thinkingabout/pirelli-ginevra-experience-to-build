(function (window) {

    class TechnologyViewCarouselOverlay extends window.AbstractViewOverlay {

        constructor(id, options) {
            super(id, options);
            this.$carousel = null;
            this.$backdrop = this.$domElement.find(".view-overlay-backdrop");
            this.$content = this.$domElement.find(".view-overlay-content");
            this.$closeBtn = this.$content.find("#technology-view-carousel-overlay__close-btn");
            this.$tab = this.$domElement.find(".technology-view-carousel-overlay__tab");
        }

        init() {

            this.$carousel = this.$tab.find('.carousel').flickity({
                autoPlay: false,
                prevNextButtons: false,
                setGallerySize: false,
                pageDots: this.$domElement.find(".view-overlay-content .carousel .carousel-cell").length > 1,
                draggable: this.$domElement.find(".view-overlay-content .carousel .carousel-cell").length > 1
            });

            this.$closeBtn.on("click", event => {
                this.hide();
            });

            super.init();
        }

        show(animated = true) {

            this.$carousel.flickity("selectCell", 0, false, true);

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.fromTo(this.$backdrop, 0.75, {
                opacity: 0
            }, {
                force3D: "auto",
                opacity: 1,
                ease: window.Cubic.easeOut
            });

            nestedTL.fromTo(this.$content, 0.65, {
                yPercent: 120
            }, {
                force3D: "auto",
                yPercent: 0,
                ease: window.Expo.easeOut
            }, 0.3);

            super.show(animated, nestedTL);
        }

        onShowStart() {

            super.onShowStart();

            //must be after "super.onShowStart()"
            this.$carousel.flickity('select', 0, false, true);
            this.$carousel.flickity("resize");
        }

        onShowComplete() {

            this.$carousel.flickity("resize");

            super.onShowComplete();
        }

        hide(animated = true) {

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.to(this.$content, 0.65, {
                force3D: "auto",
                yPercent: 120,
                ease: window.Expo.easeInOut
            });
            nestedTL.to(this.$backdrop, 0.65, {
                force3D: "auto",
                opacity: 0,
                ease: window.Cubic.easeOut
            }, 0.3);

            super.hide(animated, nestedTL);
        }

        dispose() {

            if (this.$carousel) {
                this.$carousel.flickity('destroy');
            }

            this.$closeBtn.off("click");
            this.$closeBtn = null;
            this.$backdrop = null;
            this.$content = null;
            this.$carousel = null;

            super.dispose();
        }
    }

    window.TechnologyViewCarouselOverlay = TechnologyViewCarouselOverlay;
})(window);
