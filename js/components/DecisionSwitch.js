(function (window) {

    class DecisionSwitch extends window.AbstractElement {

        constructor(id, options) {

            super(id, options);

            this.draggable = null;
            this.$scrollbar = this.$domElement.find(".decision-switch-scrollbar");
            this.$dragger = this.$domElement.find(".decision-switch-dragger");

            this.onChange = options.onChange || function () {};
        }

        init() {

            const V_MIN = 10;
            const V_MAX = 125;

            this.draggable = window.Draggable.create(this.$dragger.get(0), {
                type: "x",
                edgeResistance: 1,
                dragResistance: 0.3,
                minDuration: 0.2,
                maxDuration: 0.3,
                throwProps: true,
                bounds: {
                    left: 10,
                    width: 180
                },
                snap: value => {
                    return value > (V_MAX - V_MIN) / 2 ? V_MAX : V_MIN;
                },
                onDragStart: () => {
                    this.$domElement.attr("data-val", "");
                },
                onDragEnd: () => {},
                onThrowComplete: () => {
                    this.$domElement.attr("data-val", this.draggable[0].x > this.$scrollbar.width() / 2 ? "yes" : "no");
                    this.onChange(this.$domElement.attr("data-val"));
                }
            });

            TweenMax.set(this.$dragger, {
                x: this.$scrollbar.width() / 2 - 5,
                force3D: "true"
            });

            this.$scrollbar.on("click", event => {
                event.stopPropagation();

                let _toX;

                if (event.originalEvent.offsetX > this.$scrollbar.width() / 2) {
                    _toX = 125;
                    this.$domElement.attr("data-val", "yes");
                } else {
                    _toX = 10;
                    this.$domElement.attr("data-val", "no");
                }

                TweenMax.to(this.$dragger, 0.6, {
                    x: _toX,
                    force3D: "true",
                    ease: Expo.easeOut,
                    onComplete: () => {
                        this.draggable[0].update();
                    }
                });

                this.onChange(this.$domElement.attr("data-val"));
            });

            super.init();
        }

        dispose() {

            this.$scrollbar.off("click");

            this.$domElement.attr("data-val", "");

            if (this.draggable) {
                this.draggable[0].disable();
                this.draggable = null;
            }

            this.$dragger.removeClass("drag-active");

            TweenMax.set(this.$dragger, {
                clearProps: "all"
            });

            super.dispose();
        }
    }

    window.DecisionSwitch = DecisionSwitch;
})(window);
