(function (window) {

    class AbstractViewOverlay extends window.AbstractViewTimeline {

        constructor(id, options) {
            super(id, options);
        }

        onShowStart() {
            this.$domElement.addClass("active");
        }

        onHideComplete() {
            this.$domElement.removeClass("active");
        }
    }

    window.AbstractViewOverlay = AbstractViewOverlay;
})(window);
