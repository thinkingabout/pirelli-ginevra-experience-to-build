(function (window) {

    class AbstractModal extends window.AbstractView {

        static get SHOW_STATE() {
            return "show.bs.modal";
        }

        static get SHOWN_STATE() {
            return "shown.bs.modal";
        }

        static get HIDE_STATE() {
            return "hide.bs.modal";
        }

        static get HIDDEN_STATE() {
            return "hidden.bs.modal";
        }

        constructor(id, options) {

            const OPTIONS_DEFAULT = {
                modal: {
                    show: false, // Shows the modal when initialized.
                    focus: true, // Puts the focus on the modal when initialized.
                    keyboard: false, // Closes the modal when escape key is pressed
                    backdrop: true // Includes a modal-backdrop element. Alternatively, specify static for a backdrop which doesn't close the modal on click.
                }
            };

            options = $.extend(true, OPTIONS_DEFAULT, _.clone(options));

            super(id, options);

            this.state = this.options.modal.show === true ? window.AbstractModal.SHOWN_STATE : window.AbstractModal.HIDDEN_STATE;

            this.$domElement.modal(this.options.modal);

            this.$domElement.on(window.AbstractModal.SHOW_STATE, event => {
                this.state = window.AbstractModal.SHOW_STATE;
                this.modalShowHandler();
            });

            this.$domElement.on(window.AbstractModal.SHOWN_STATE, event => {
                this.state = window.AbstractModal.SHOWN_STATE;
                this.modalShownHandler();
            });

            this.$domElement.on(window.AbstractModal.HIDE_STATE, event => {
                this.state = window.AbstractModal.HIDE_STATE;
                this.modalHideHandler();
            });

            this.$domElement.on(window.AbstractModal.HIDDEN_STATE, event => {
                this.state = window.AbstractModal.HIDDEN_STATE;
                this.modalHiddenHandler();
            });

            this.$modalContent = this.$domElement.find(".modal-content");
        }

        init() {

            this.$domElement.on("show.bs.modal." + this.id, event => {
                super.triggerShowStart();
            });

            this.$domElement.on("shown.bs.modal." + this.id, event => {
                super.triggerShowComplete();
            });

            this.$domElement.on("hide.bs.modal." + this.id, event => {
                super.triggerHideStart();
            });

            this.$domElement.on("hidden.bs.modal." + this.id, event => {
                super.triggerHideComplete();
            });

            super.init();
        }

        modalShowHandler(event = null) {
            ;
        }

        modalShownHandler(event = null) {
            ;
        }

        modalHideHandler(event = null) {
            ;
        }

        modalHiddenHandler(event = null) {
            ;
        }

        show() {
            if (this.showStatus) return;

            this.$domElement.modal("show");
        }

        hide() {
            if (this.hideStatus) {
                return;
            }

            this.$domElement.modal("hide");
        }

        toggle() {
            this.$domElement.modal("toggle");
        }

        update(data = {}) {
            this.handleUpdate();
            super.update();
        }

        handleUpdate() {
            this.$domElement.modal("handleUpdate");
        }

        dispose() {
            this.$domElement.off("show.bs.modal shown.bs.modal hide.bs.modal hidden.bs.modal");
            this.$modalContent = null;
            super.dispose();
        }
    }

    window.AbstractModal = AbstractModal;
})(window);
