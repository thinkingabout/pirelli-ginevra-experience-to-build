(function (window) {

    class PirelliWheel extends window.AbstractElement {

        static get TECHNOLOGY_MODE() {
            return "technology-mode";
        }

        static get COLORS_MODE() {
            return "colors-mode";
        }

        static get SURVEY_SETUP_MODE() {
            return "survey-setup-mode";
        }

        constructor(id, options) {

            super(id, options);

            this.$wheel = this.$domElement.find("#pirelli-wheel");
            this.$technology = this.$domElement.find(".pirelli-wheel__technology");
            this.$colors = this.$domElement.find(".pirelli-wheel__colors");
            this.$wheelBorder = this.$domElement.find("#pirelli-wheel__border");
            this.$donut = this.$domElement.find("#pirelli-wheel-pzero-customizable svg path");

            this.$wheelBorderSvgCircle1 = this.$wheelBorder.find("svg circle:first-of-type");
            this.$wheelBorderSvgCircle2 = this.$wheelBorder.find("svg circle:last-of-type");

            this.WHEEL_RADIUS = this.$wheel.width() / 2 + 30;
            this.BORDER_RADIUS = this.$wheelBorder.width() / 2;

            this._currentColorSelection = null;
            this._pZeroType = false;
            this._customColors = false;

            this._wheelmode = "";
        }

        init() {

            /*
             * COLOR MODE: Static triggers position
             */

            let $colorTriggers = this.$domElement.find(".pirelli-wheel__colors .pirelli-wheel__border-trigger");
            let STEP = 360 / $colorTriggers.length;
            this.COLORS_DEGREES = $colorTriggers.map((i, el) => {
                let deg = STEP * i;
                $(el).attr("data-deg", deg);
                // add an extra (STEP / 2) to matxch the layout
                let rad = Math.PI + deg * Math.PI / 2;
                $(el).css({
                    transform: "translate3d(" + Math.sin(rad) * this.BORDER_RADIUS + "px, " + Math.cos(rad) * this.BORDER_RADIUS + "px, 0)"
                });
                //
                return parseFloat(deg);
            }).toArray();

            /*
             * TECHNOLOGY MODE: Static cue points position
             */

            let $pointsContainer = this.$domElement.find(".pirelli-wheel__technology  .pirelli-wheel__cue-point-container");
            $pointsContainer.map((i, el) => {
                const D = parseInt($(el).attr("data-deg"));
                $(el).find(".pirelli-wheel__cue-point__icon").attr("data-parent-deg", D);
                $(el).find(".pirelli-wheel__cue-point__icon").css("transform", "rotateZ(" + -D % 90 + "deg)");
                $(el).css("transform", "rotateZ(" + D + "deg)");
                return 360 - D;
            }).toArray();

            super.init();
        }

        set pZeroType(b) {

            if (b === this._pZeroType) {
                return;
            }

            this._pZeroType = b;

            let wheelSwitchTL = new window.TimelineMax();

            wheelSwitchTL.fromTo(this.$wheel.find("#pirelli-wheel-grpx"), 0.8, {
                rotation: 0
            }, {
                force3D: "auto",
                rotation: this._pZeroType ? 360 : -360,
                ease: window.Power2.easeOut
            });

            wheelSwitchTL.to(this.$wheel.find("#pirelli-wheel-pzero-default"), 0.3, {
                force3D: "auto",
                opacity: this._pZeroType ? 0 : 1,
                ease: window.Expo.easeOut
            }, 0.25);

            wheelSwitchTL.to(this.$wheel.find("#pirelli-wheel-pzero-customizable"), 0.3, {
                force3D: "auto",
                opacity: this._pZeroType ? 1 : 0,
                ease: window.Expo.easeOut
            }, 0.25);
        }

        get customColors() {
            return this._customColors;
        }

        set customColors(b) {

            if (b === this._customColors) {
                return;
            }

            if (this.customColorsTL) {
                this.customColorsTL.kill();
            }

            this.customColorsTL = new window.TimelineMax();

            if (b) {

                if (this.drg) {
                    this.drg[0].disable();
                }

                this.customColorsTL.staggerTo(this.$wheelBorder.find(".pirelli-wheel__colors .pirelli-wheel__border-trigger"), 0.6, {
                    scale: 0,
                    transformOrigin: "center",
                    ease: Expo.easeInOut
                }, 0.1);

                this.customColorsTL.to(this.$wheelBorder.find(".pirelli-wheel__colors .pirelli-wheel__cue-point"), 0.6, {
                    scale: 0,
                    ease: Expo.easeOut
                }, 0);

                this.customColorsTL.to(this.$wheelBorder, 0.6, {
                    autoAlpha: 0,
                    ease: Expo.easeOut
                }, 0);

                this.customColorsTL.to(this.$domElement.find(".pirelli-wheel__colors:not(#pirelli-wheel-grpx__colors-container)"), 0.6, {
                    autoAlpha: 0,
                    ease: Expo.easeOut
                }, 0);
            } else {

                if (this.drg) {
                    this.drg[0].enable();
                }

                this.customColorsTL.staggerTo(this.$wheelBorder.find(".pirelli-wheel__colors .pirelli-wheel__border-trigger"), 0.6, {
                    scale: 1,
                    transformOrigin: "center",
                    ease: Expo.easeInOut
                }, 0.1);

                this.customColorsTL.to(this.$wheelBorder.find(".pirelli-wheel__colors .pirelli-wheel__cue-point"), 0.6, {
                    scale: 1,
                    ease: Expo.easeOut
                }, 0);

                this.customColorsTL.to(this.$wheelBorder, 0.6, {
                    autoAlpha: 1,
                    ease: Expo.easeInOut
                }, 0);

                this.customColorsTL.to(this.$domElement.find(".pirelli-wheel__colors:not(#pirelli-wheel-grpx__colors-container)"), 0.6, {
                    autoAlpha: 1,
                    ease: Expo.easeOut
                }, 0);

                let selectedIndex = $(".pirelli-wheel__colors .pirelli-wheel__border-trigger.hit").index();
                $(this.$wheelBorder.find(".pirelli-wheel__colors .pirelli-wheel__border-trigger")[selectedIndex]).trigger("click", [true]);
            }

            this._customColors = b;
        }

        resetRotation() {

            let $cueIcon = this.$domElement.find(".pirelli-wheel__colors #dragger.pirelli-wheel__cue-point .pirelli-wheel__cue-point__icon");

            this.throwCompletePosition = null;

            if (this.$wheel.css("transform")) {
                TweenMax.to(this.$wheel, 0.85, {
                    directionalRotation: "0_short",
                    clearProps: "transform",
                    ease: Expo.easeInOut,
                    onUpdate: this._wheelmode == PirelliWheel.COLORS_MODE ? () => {
                        $cueIcon.css("transform", "rotateZ(" + -getRotation(this.$wheel) + "deg)");
                    } : null,
                    onComplete: this._wheelmode == PirelliWheel.COLORS_MODE ? () => {
                        if (this.drg[0]) {
                            this.drg[0].update();
                        }
                    } : null
                });
            }
        }

        set mode(_mode) {

            this._wheelmode = _mode;

            if (this.drg) {

                this.drg[0].kill();

                /*TweenMax.set(this.$wheel, {
                    clearProps: "transform"
                })*/

                this.resetRotation();
            }

            this.$domElement.removeClass("survey-setup-mode");
            this.$domElement.removeClass("technology-mode");
            this.$domElement.removeClass("colors-mode");

            this.drg = null;
            this.degrees = null;
            this.customColors = false;

            if (_mode == PirelliWheel.SURVEY_SETUP_MODE) {
                this.$domElement.addClass("survey-setup-mode");
                this._setUpSurveyMode();
            } else if (_mode == PirelliWheel.TECHNOLOGY_MODE) {
                this.$domElement.addClass("technology-mode");
                this._setUpTechnologyMode();
            } else if (_mode == PirelliWheel.COLORS_MODE) {
                this.$domElement.addClass("colors-mode");
                this._setUpColorsMode();
            }

            /* Move the bg outside to avoid the rotation */

            if (_mode == PirelliWheel.COLORS_MODE) {
                $("#pirelli-wheel-grpx").appendTo("#pirelli-wheel-grpx__colors-container");
            } else {
                $("#pirelli-wheel-grpx").prependTo("#pirelli-wheel");
            }
        }

        get currentColorSelection() {
            return this._currentColorSelection;
        }

        setCustomColor(color, data) {
            this.$donut.css("fill", color);
            this._currentColorSelection = {
                hex: color,
                data: data,
                custom: true
            };
        }

        dispose() {
            this.$wheel = null;
            this.$technology = null;
            this.$colors = null;
            this.$wheelBorder = null;
            this.$wheelBorderSvgCircle1 = null;
            this.$wheelBorderSvgCircle2 = null;
            this.mode = null;
            super.dispose();
        }

        _setUpSurveyMode() {}

        _setUpTechnologyMode() {
            let $cues = this.$domElement.find(".pirelli-wheel__technology .pirelli-wheel__cue-point");
            $cues.on("click", event => {
                this.onTechnologyPointSelected();
            });
        }

        _setUpColorsMode() {

            this.pZeroType = true;

            this.throwCompletePosition = null;

            this.$wheelBorderSvgCircle1.css("stroke", "#5a5a5a");
            this.$wheelBorderSvgCircle2.css("stroke", "#5a5a5a");

            let $cue = this.$domElement.find(".pirelli-wheel__colors #dragger.pirelli-wheel__cue-point");
            let $triggers = this.$domElement.find(".pirelli-wheel__colors .pirelli-wheel__border-trigger");

            let $cueFill = $cue.find("#dragger-fill");
            let $cueBorder = $cue.find(".dragger-border");
            let $cueIcon = $cue.find(".pirelli-wheel__cue-point__icon");
            let $borderMask = this.$domElement.find("#wheel__border-mask");
            let $draggerArrows = this.$domElement.find("#dragger-arrows");

            $cueIcon.css("transform", "");

            this.degrees = this.COLORS_DEGREES;

            let _self = this;

            $triggers.off("click").on("click", (event, avoidAnimation) => {

                selectIndex($(event.delegateTarget).index());

                // cursor

                TweenMax.to(this.$wheel, avoidAnimation ? 0 : 0.85, {
                    directionalRotation: $(event.delegateTarget).data("deg") + "_short",
                    ease: Expo.easeInOut
                });

                // mask

                TweenMax.to($borderMask, avoidAnimation ? 0 : 0.85, {
                    transformOrigin: "center",
                    directionalRotation: $(event.delegateTarget).data("deg") - 180 + "_short",
                    ease: Expo.easeInOut
                });
            });

            this.drg = Draggable.create(this.$wheel, {
                type: "rotation",
                throwProps: true,
                snap: this._snap.bind(this),
                onDrag: v => {
                    $cue.css("background-color", "");
                    $cueIcon.css("transform", "rotateZ(" + -this.drg[0].rotation + "deg)");
                    updateMask(this.drg[0].rotation);
                },
                onDragStart: v => {
                    $cue.addClass("dragging");
                },
                onDragEnd: v => {
                    $cue.removeClass("dragging");
                },
                onThrowUpdate: v => {

                    $cueIcon.css("transform", "rotateZ(" + -this.drg[0].rotation + "deg)");

                    let hits = false;

                    $triggers.each((i, el) => {
                        if (Draggable.hitTest(el, $cue[0])) {
                            $($triggers[i]).addClass("hit");
                            hits = i;
                        } else {
                            $($triggers[i]).removeClass("hit");
                        }
                    });

                    if (hits !== false) {
                        $cueFill.css("fill", $($triggers[hits]).css("background-color"));
                        $cueBorder.css("stroke", $($triggers[hits]).css("background-color"));
                        _self.$donut.css("fill", $($triggers[hits]).css("background-color"));
                    } else {
                        // reset dragger color here
                    }

                    updateMask(this.drg[0].rotation);
                },
                onThrowComplete: v => {

                    let $hit = $triggers.filter((i, el) => {
                        return $(el).hasClass("hit");
                    });

                    if ($hit.length) {
                        let $h = $($hit[0]);
                        this._currentColorSelection = {
                            hex: $h.css("background-color"),
                            data: $h.attr("data-code"),
                            custom: false
                        };
                    }

                    _self.throwCompletePosition = parseInt(_self.drg[0].rotation);
                }
            });

            this.throwCompletePosition = parseInt(getRotation(this.$wheel));

            /* Trigger the topmost point  */

            $($triggers[0]).trigger("click", [true]);

            function selectIndex(i) {
                $($triggers[i]).addClass("hit");
                $cueFill.css("fill", $($triggers[i]).css("background-color"));
                $cueBorder.css("stroke", $($triggers[i]).css("background-color"));
                _self.$donut.css("fill", $($triggers[i]).css("background-color"));
                _self._currentColorSelection = {
                    hex: $($triggers[i]).css("background-color"),
                    data: $($triggers[i]).attr("data-code"),
                    custom: false
                };
            }

            function updateMask(deg) {
                TweenMax.set($borderMask, {
                    transformOrigin: "center",
                    rotation: deg - 180 + "deg"
                });
            }
        }

        _snap(originalValue) {

            if (this.throwCompletePosition == parseInt(originalValue)) {
                return originalValue;
            }

            // suppress originalValue to 0-360
            var endValue = Math.round(originalValue % 360);
            // change negative degree to positive
            if (endValue < 0) {
                endValue = 360 + endValue;
            }
            // get closest value from array
            var arrayValue = parseFloat(closest(endValue, this.degrees));
            // get extra rotations to add to our arrayValue
            var addValue = Math.floor((originalValue - arrayValue) / 360) * 360;
            // adjust for shortest rotation
            if ((endValue - arrayValue + 360) % 360 > 180) {
                addValue += 360;
            }

            return arrayValue + addValue;
        }
    }

    window.PirelliWheel = PirelliWheel;

    /**
     *   Internal use only
     */

    function difference(a, b) {
        var d = Math.abs(a - b);
        return d > 180 ? 360 - d : d;
    };

    function closest(a, bs) {
        var ds = bs.map(function (b) {
            return difference(a, b);
        });
        return bs[ds.indexOf(Math.min.apply(null, ds))];
    };

    function getRotation($el) {

        const TR = $el.css("transform");

        if (TR == "none") {
            return;
        }

        let values = TR.split('(')[1];
        values = values.split(')')[0];
        values = values.split(',');

        const A = values[0];
        const B = values[1];
        const C = values[2];
        const D = values[3];

        const SCALE = Math.sqrt(A * A + B * B);

        const SIN = B / SCALE;
        const ANGLE = Math.round(Math.asin(SIN) * (180 / Math.PI));

        return ANGLE;
    }
})(window);
