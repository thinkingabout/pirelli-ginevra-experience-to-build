(function (window) {

    class AppStaticData {

        constructor() {}

        static get BRANDS_LIST() {
            return [{
                name: "ACURA",
                json: "resources/data/brands/acura.json"
            }, {
                name: "ALFA ROMEO",
                json: "resources/data/brands/alfa-romeo.json"
            }, {
                name: "ALPINA",
                json: "resources/data/brands/alpina.json"
            }, {
                name: "ASTON MARTIN",
                json: "resources/data/brands/aston-martin.json"
            }, {
                name: "AUDI",
                json: "resources/data/brands/audi.json"
            }, {
                name: "BENTLEY",
                json: "resources/data/brands/bentley.json"
            }, {
                name: "BMW",
                json: "resources/data/brands/bmw.json"
            }, {
                name: "BUGATTI",
                json: "resources/data/brands/bugatti.json"
            }, {
                name: "CADILLAC",
                json: "resources/data/brands/cadillac.json"
            }, {
                name: "FERRARI",
                json: "resources/data/brands/ferrari.json"
            }, {
                name: "FISKER",
                json: "resources/data/brands/fisker.json"
            }, {
                name: "HUMMER",
                json: "resources/data/brands/hummer.json"
            }, {
                name: "INFINITI",
                json: "resources/data/brands/infiniti.json"
            }, {
                name: "JAGUAR",
                json: "resources/data/brands/jaguar.json"
            }, {
                name: "LAMBORGHINI",
                json: "resources/data/brands/lamborghini.json"
            }, {
                name: "LANCIA",
                json: "resources/data/brands/lancia.json"
            }, {
                name: "LAND ROVER",
                json: "resources/data/brands/land-rover.json"
            }, {
                name: "LEXUS",
                json: "resources/data/brands/lexus.json"
            }, {
                name: "LINCOLN",
                json: "resources/data/brands/lincoln.json"
            }, {
                name: "LOTUS",
                json: "resources/data/brands/lotus.json"
            }, {
                name: "MASERATI",
                json: "resources/data/brands/maserati.json"
            }, {
                name: "MCLAREN",
                json: "resources/data/brands/mclaren.json"
            }, {
                name: "MERCEDES",
                json: "resources/data/brands/mercedes.json"
            }, {
                name: "MINI",
                json: "resources/data/brands/mini.json"
            }, {
                name: "MORGAN",
                json: "resources/data/brands/morgan.json"
            }, {
                name: "PAGANI",
                json: "resources/data/brands/pagani.json"
            }, {
                name: "PORSCHE",
                json: "resources/data/brands/porsche.json"
            }, {
                name: "ROLLS ROYCE",
                json: "resources/data/brands/rolls-royce.json"
            }, {
                name: "TESLA",
                json: "resources/data/brands/tesla.json"
            }, {
                name: "VOLVO",
                json: "resources/data/brands/volvo.json"
            }];
        }

        static init() {
            ;
        }

        static getBrand(brand) {
            for (let i = 0, len = window.AppStaticData.BRANDS_LIST.length; i < len; i++) {
                if (window.AppStaticData.BRANDS_LIST[i].name == brand.toUpperCase()) {
                    return window.AppStaticData.BRANDS_LIST[i];
                }
            }
        }
    }

    window.AppStaticData = AppStaticData;
})(window);
