(function (window) {

    class Globals {

        constructor() {}

        static init() {}

        /**
         *   Section ID/URL
         */

        static get HOME() {
            return "";
        }

        static get USERS_LIST_VIEW() {
            return "users-list";
        }

        static get WELCOME_VIEW() {
            return "welcome";
        }

        static get DESIGN_INTRO_VIEW() {
            return "design-intro";
        }

        static get DESIGN_VIEW() {
            return "design";
        }

        static get COLOR_VIEW() {
            return "color";
        }

        static get CODEVELOPMENT_VIEW() {
            return "codevelopment";
        }

        static get TYRES_RANGE_VIEW() {
            return "tyres-range";
        }

        static get TECHNOLOGY_VIEW() {
            return "technology";
        }

        static get CONNESSO_VIEW() {
            return "connesso";
        }

        static get PZERO_VIEW() {
            return "pzero";
        }

        static get SERVICES_VIEW() {
            return "services";
        }

        static get SURVEY_SETUP_VIEW() {
            return "survey-setup";
        }

        static get THANKYOU_VIEW() {
            return "thankyou";
        }

        /**
         *   Survey Path
         */

        static setBrandData(data) {
            Globals._BRAND_DATA = data;
        }

        static get BRAND_DATA() {

            if (!Globals._BRAND_DATA) {
                return {
                    "general": {
                        "brand": "Porsche",
                        "is_prestige": true,
                        "logo": "resources/brand-logo/large/logo_porsche.png",
                        "wheel_logo": "resources/brand-logo/small/logo_porsche.png"
                    },

                    "view": {
                        "design_intro": {
                            "img_src": "resources/design-intro-view/porsche/img.jpg"
                        },

                        "design": {
                            "items": [{
                                "years": "2017 - 2018",
                                "desc_html": "Lorem iptewhsum dolor sit amet, <strong>consectetur</strong> adipiscing elit, sed do eiusmod tempor incididunt ut <strong>labore et dolore</strong> magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco <strong>laboris nisi</strong> ut aliquip ex ea commodo consequat. ",
                                "car": "resources/design-view/porsche/slide-1/car.jpg"
                            }, {
                                "years": "2013",
                                "desc_html": "Lorem ipstewhum dolor sit amet, <strong>consectetur</strong> adipiscing elit, sed do eiusmod tempor incididunt ut <strong>labore et dolore</strong> magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco <strong>laboris nisi</strong> ut aliquip ex ea commodo consequat. ",
                                "car": "resources/design-view/porsche/slide-1/car.jpg"
                            }, {
                                "years": "2011",
                                "desc_html": "Lorem ipstewhum dolor sit amet, <strong>consectetur</strong> adipiscing elit, sed do eiusmod tempor incididunt ut <strong>labore et dolore</strong> magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco <strong>laboris nisi</strong> ut aliquip ex ea commodo consequat. ",
                                "car": "resources/design-view/porsche/slide-1/car.jpg"
                            }, {
                                "years": "2006 - 2009",
                                "desc_html": "Lorem ietwhwtepsum dolor sit amet, <strong>consectetur</strong> adipiscing elit, sed do eiusmod tempor incididunt ut <strong>labore et dolore</strong> magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco <strong>laboris nisi</strong> ut aliquip ex ea commodo consequat. ",
                                "car": "resources/design-view/porsche/slide-1/car.jpg"
                            }, {
                                "years": "2004",
                                "desc_html": "Lorem ipstwhtum dolor sit amet, <strong>consectetur</strong> adipiscing elit, sed do eiusmod tempor incididunt ut <strong>labore et dolore</strong> magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco <strong>laboris nisi</strong> ut aliquip ex ea commodo consequat. ",
                                "car": "resources/design-view/porsche/slide-1/car.jpg"
                            }]
                        },

                        "co_development": {
                            "mark": {
                                "src": "resources/codevelopement-view/marks/marcatura_N0.png",
                                "default": "n0",
                                "list": ["n0", "n1", "n2", "n3", "n4", "nx"]
                            },
                            "slide1": {
                                "bg": "resources/codevelopement-view/porsche/slide-1/bg.jpg"
                            },
                            "slide2": {
                                "bg": "resources/codevelopement-view/porsche/slide-2/bg.jpg"
                            }
                        },

                        "tyres_range": {
                            "slide1": "resources/tyres-range-view/porsche/slide-1.png",
                            "slide2": "resources/tyres-range-view/porsche/slide-2.png"
                        },

                        "technology": {
                            "table": ["resources/technology-view/table/porsche/slide-1.jpg", "resources/technology-view/table/porsche/slide-2.jpg", "resources/technology-view/table/porsche/slide-3.jpg", "resources/technology-view/table/porsche/slide-4.jpg", "resources/technology-view/table/porsche/slide-5.jpg", "resources/technology-view/table/porsche/slide-6.jpg", "resources/technology-view/table/porsche/slide-7.jpg", "resources/technology-view/table/porsche/slide-8.jpg", "resources/technology-view/table/porsche/slide-9.jpg", "resources/technology-view/table/porsche/slide-10.jpg", "resources/technology-view/table/porsche/slide-11.jpg", "resources/technology-view/table/porsche/slide-12.jpg", "resources/technology-view/table/porsche/slide-13.jpg", "resources/technology-view/table/porsche/slide-14.jpg", "resources/technology-view/table/porsche/slide-15.jpg"],
                            "slider": {
                                "pncs": true,
                                "sealinside": true,
                                "runflat": true
                            }
                        }
                    }
                };
            }

            return Globals._BRAND_DATA;
        }

        /**
         *   Survey Path
         */

        static get SURVEY_PATH() {
            return Globals._SURVEY_PATH ? Globals._SURVEY_PATH : "";
        }

        static set SURVEY_PATH(path) {
            Globals._SURVEY_PATH = path.join();
        }

        /**
         *   Methods
         */

        static checkConnection() {

            if (!navigator.connection || !navigator.connection.type) {
                console.warn('Cannot check the internet connection!');
                return !Globals.isRealDeviceApp;
            }

            const NETWORK_STATE = navigator.connection.type;

            /*alert("navigator.connection.type: " + navigator.connection.type);*/

            /*const STATES = {};
            STATES[Connection.UNKNOWN] = 'Unknown connection';
            STATES[Connection.ETHERNET] = 'Ethernet connection';
            STATES[Connection.WIFI] = 'WiFi connection';
            STATES[Connection.CELL_2G] = 'Cell 2G connection';
            STATES[Connection.CELL_3G] = 'Cell 3G connection';
            STATES[Connection.CELL_4G] = 'Cell 4G connection';
            STATES[Connection.CELL] = 'Cell generic connection';
            STATES[Connection.NONE] = 'No network connection';*/

            //return STATES[NETWORK_STATE];

            /*alert("NETWORK_STATE: " + NETWORK_STATE);
            alert("Connection.NONE: " + Connection.NONE);*/

            return NETWORK_STATE != Connection.NONE;
        }

        static set currentUser(user) {
            Globals._currentUser = user;
        }

        static get currentUser() {

            if (!Globals._currentUser) {

                /*
                 *   Development only!
                 */

                return {
                    user_id: "1234567",
                    email: "lorem.ipsum@gmail.com",
                    first_name: "Lorem",
                    last_name: "Ipsum",
                    vehicle_brand: "ALFA ROMEO"
                };
            }

            return Globals._currentUser;
        }

        static get isRealDeviceApp() {
            //return window.Detectizr.device.type == "tablet" && window.location.hostname != "localhost";
            return !!window.cordova;
        }

        /**
         *   Local user object get/set
         */

        static setUserData(att, value) {

            console.log("Saving data for user: ", Globals.currentUser);

            if (!Globals.currentUser) {
                console.warn('Cannot save data for the current user. Please select a user.');
                return;
            }

            const ID = Globals.currentUser.user_id;

            if (!Globals._userData) {
                Globals._userData = {};
            }

            Globals._userData[att] = value;

            /*if (!Globals.currentUser) {
                console.warn('Cannot save data for the current user. Please select a user.');
                return;
            }
             const ID = Globals.currentUser.user_id;
             let userData = JSON.parse(window.localStorage.getItem("user_data"));
             if (!userData) {
                userData = {};
            }
             if (!userData[ID]) {
                userData[ID] = {};
            }
             userData[ID][att] = value;
             window.localStorage.setItem('user_data', JSON.stringify(userData));*/
        }

        static getUserData(att) {

            console.log("Retrieving data for user: ", Globals.currentUser);

            if (!att) {
                console.warn('getUserData invalid type [' + att + '].');
                return null;
            }

            if (!Globals.currentUser) {
                console.warn('No user selected. Cannot retrieve data for the current user.');
                return null;
            }

            const ID = Globals.currentUser.user_id;

            if (!Globals._userData) {
                console.warn('No data saved for the current user.');
                return null;
            }

            if (Globals._userData[att]) {
                return Globals._userData[att];
            } else {
                console.warn('Value [' + att + '] has not been saved for the current user.');
                return null;
            }

            /*console.log("Retrieving data for user: ", Globals.currentUser);
             if (!Globals.currentUser) {
                console.warn('No user selected. Cannot retrieve data for the current user.');
                return null;
            }
             const ID = Globals.currentUser.user_id;
             const USER_DATA = JSON.parse(window.localStorage.getItem("user_data"));
             if (!USER_DATA) {
                console.warn('No data saved.');
                return null;
            }
             if (!USER_DATA[ID]) {
                console.warn('No data saved for the current user.');
                return null;
            }
             if (att) {
                 if (!USER_DATA[ID][att]) {
                    console.warn('Value [' + att + '] has not been saved for the current user.');
                    return null;
                } else {
                    return USER_DATA[ID][att];
                }
             } else {
                return USER_DATA[ID];
            }*/
        }

        /**
         *   Storage user object and list
         */

        static resetStorageUserData() {

            console.log("Reset data for user: ", Globals.currentUser);

            if (!Globals.currentUser) {
                console.warn('Cannot save data for the current user. Please select a user.');
                return;
            }

            const ID = Globals.currentUser.user_id;

            let userData = JSON.parse(window.localStorage.getItem("user_data"));

            if (!userData) {
                return;
            }

            if (!userData[ID]) {
                return;
            }

            delete userData[ID];

            window.localStorage.setItem('user_data', JSON.stringify(userData));
        }

        static getStorageData() {

            const USER_DATA = JSON.parse(window.localStorage.getItem("user_data"));

            if (!USER_DATA) {
                console.warn('No data saved.');
                return null;
            }

            return USER_DATA;
        }

        static registerStorageUserData() {

            if (!Globals.currentUser) {
                console.warn('Cannot save data for the current user. Please select a user.');
                return;
            }

            if (!Globals._userData) {
                console.warn('Data for current user are not available.');
                return;
            }

            const ID = Globals.currentUser.user_id;

            let userData = JSON.parse(window.localStorage.getItem("user_data"));

            if (!userData) {
                userData = {};
            }

            userData[ID] = Globals._userData;

            window.localStorage.setItem('user_data', JSON.stringify(userData));
        }

        static dispose() {}
    }

    window.Globals = Globals;
})(window);
