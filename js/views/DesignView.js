(function (window) {

    class DesignView extends window.AbstractViewTimeline {

        static get TEMPLATE() {
            const TPL = $("#tpl__design-view").text();
            const COMPILED = _.template(TPL);
            return COMPILED({
                general: Globals.BRAND_DATA.general,
                data: Globals.BRAND_DATA.view.design
            });
        }

        constructor(id, options) {

            const OPTIONS_MANDATORY = {
                watchResize: true
            };

            options = $.extend(true, _.clone(options), OPTIONS_MANDATORY);

            super(id, options);

            this.$carousel = null;
            this.$bg = this.$domElement.find(".view-bg");
            this.$header = this.$domElement.find(".view-header");
            this.$footer = this.$domElement.find(".view-footer");
            this.$yearsSlider = this.$domElement.find("#design-view__years-slider");
        }

        init() {

            super.init();

            let _self = this;

            this.$carousel = this.$domElement.find(".view-content .view-main .carousel").flickity({
                autoPlay: false,
                prevNextButtons: false,
                setGallerySize: false,
                pageDots: false
            });

            this.resizeHandler();

            /* slider buttons */

            this.$slider = this.$domElement.find("#design-view__years-slider");
            this.$cursor = this.$domElement.find("#design-view__years-slider__cursor");

            this.$buttons = this.$domElement.find("li");
            this.$buttons.on("click", event => {
                this.$carousel.flickity('select', $(event.delegateTarget).index());
                alignCursor($(event.delegateTarget).index());
            });

            //alignCursor(0, false);

            function alignCursor(index, animated = true) {
                _self.$buttons.removeClass("is-selected");
                $(_self.$buttons[index]).addClass("is-selected");
                TweenMax.to(_self.$cursor, animated ? 0.65 : 0, {
                    x: $(_self.$buttons[index]).offset().left - _self.$slider.offset().left,
                    force3D: "auto",
                    ease: Circ.easeOut
                });
            }

            this.$domElement.one(AbstractElement.EL_SHOW_COMPLETE, event => {
                this.$carousel.on('select.flickity', event => {
                    var flkty = this.$carousel.data('flickity');
                    alignCursor(flkty.selectedIndex);
                });
            });
        }

        resizeHandler(event = null) {
            if (this.$carousel) {
                this.$carousel.css("height", this.$domElement.find(".view-content .view-main").height() - 150);
                this.$carousel.flickity("resize");
            }
        }

        show(isAnimated = true) {

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.set(this.$carousel, { visibility: "hidden" });
            nestedTL.fromTo(this.$bg, 0.85, { backgroundColor: "black" }, { force3D: "auto", backgroundColor: "white", ease: window.Expo.easeInOut });
            nestedTL.addLabel("step1");

            if (Globals.BRAND_DATA.general.is_prestige) {
                nestedTL.fromTo(this.$header.find("span.v-line"), 0.85, { backgroundColor: "white" }, { force3D: "auto", backgroundColor: "black", ease: window.Expo.easeInOut }, 0);
                nestedTL.fromTo(this.$header.find("h1"), 0.85, { color: "white" }, { force3D: "auto", color: "black", ease: window.Expo.easeInOut }, 0);
            } else {
                nestedTL.fromTo(this.$header, 0.8, { yPercent: -110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Expo.easeInOut }, "step1");
            }
            nestedTL.set(this.$carousel, { visibility: "visible" }, "step1");
            nestedTL.fromTo(this.$carousel.find(".carousel-cell.is-selected p"), 0.65, { y: 50, opacity: 0 }, { force3D: "auto", y: 0, opacity: 1, ease: window.Cubic.easeOut }, "step1");
            nestedTL.fromTo(this.$carousel.find(".carousel-cell.is-selected img"), 0.85, { xPercent: 110 }, { force3D: "auto", xPercent: 0, ease: window.Cubic.easeOut }, "step1");

            nestedTL.fromTo(this.$yearsSlider.find("ul"), 0.7, { transformOrigin: "left center", scaleX: 0 }, { force3D: "auto", scaleX: 1, ease: window.Cubic.easeOut, clearProps: "all" }, "step1");
            nestedTL.staggerFromTo(this.$yearsSlider.find("ul li"), 0.6, { scale: 0 }, { force3D: "auto", scale: 1, ease: window.Cubic.easeOut, clearProps: "all" }, 0.1, "-=0.8");
            nestedTL.fromTo(this.$yearsSlider.find("#design-view__years-slider__cursor-icon"), 0.45, { opacity: 0 }, { opacity: 1, clearProps: "opacity", ease: window.Cubic.easeOut }, "-=0.5");
            nestedTL.fromTo(this.$footer, 0.85, { yPercent: 110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Cubic.easeOut }, "-=0.5");

            super.show(isAnimated, nestedTL);
        }

        hide(isAnimated = true) {

            this.resizeHandler = () => {};

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.addLabel("step1");

            nestedTL.to(this.$footer, 0.65, { force3D: "auto", yPercent: 110, opacity: 0, ease: window.Expo.easeInOut }, "step1");
            nestedTL.to(this.$header, 0.65, { force3D: "auto", yPercent: -110, opacity: 0, ease: window.Expo.easeInOut }, "step1");

            nestedTL.to(this.$carousel.find(".carousel-cell.is-selected img"), 1, { force3D: "auto", xPercent: -110, ease: window.Back.easeIn }, "step1");
            nestedTL.to(this.$carousel.find(".carousel-cell.is-selected p"), 1, { force3D: "auto", y: 50, opacity: 0, ease: window.Expo.easeInOut }, "step1");

            nestedTL.set(this.$carousel, { visibility: "hidden" });

            nestedTL.to(this.$yearsSlider.find("#design-view__years-slider__cursor-icon"), 0.85, { opacity: 0, ease: window.Expo.easeInOut }, "step1");
            nestedTL.to(this.$yearsSlider.find("ul li"), 0.5, { force3D: "auto", scale: 0, ease: Expo.easeInOut }, "step1");
            nestedTL.to(this.$yearsSlider.find("ul"), 0.5, { transformOrigin: "left center", force3D: "auto", scaleX: 0, ease: window.Expo.easeInOut }, "-=0.4");
            nestedTL.to(this.$bg, 0.8, { force3D: "auto", backgroundColor: "black", ease: window.Expo.easeInOut });

            super.hide(isAnimated, nestedTL);
        }

        dispose() {

            if (this.$carousel) {
                this.$carousel.flickity("destroy");
                this.$carousel = null;
            }

            this.$bg = null;
            this.$header = null;
            this.$footer = null;

            super.dispose();
        }

    }

    window.DesignView = DesignView;
})(window);
