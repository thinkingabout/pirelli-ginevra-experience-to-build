(function (window) {

    class SurveySetupView extends window.AbstractViewTimeline {

        static get TEMPLATE() {
            const TPL = $("#tpl__survey-setup-view").text();
            const COMPILED = _.template(TPL);
            return COMPILED({
                general: Globals.BRAND_DATA.general,
                username: Globals.currentUser.first_name + " " + Globals.currentUser.last_name,
                next_href: "#" + Globals.SERVICES_VIEW
            });
        }

        constructor(id, options) {

            super(id, options);

            this.$wheelContainer = $("#pirelli-wheel-container");
            this.$switch = this.$domElement.find(".decision-switch");
            this.$bg = this.$domElement.find(".view-bg");
            this.$questions = this.$domElement.find("#survey-setup-view__questions");
            this.$question1 = this.$questions.find("#survey-setup__question-1");
            this.$question2 = this.$questions.find("#survey-setup__question-2");
            this.$question3 = this.$questions.find("#survey-setup__question-3");
            this.$header = this.$domElement.find(".view-header");
            this.$footer = this.$domElement.find(".view-footer");

            this.onSurveyChanged = options.onSurveyChanged || function () {};

            this._labels = null;

            this.$switch.each((i, el) => {
                this.addModules(new window.DecisionSwitch(this.id + "__decision-switch-" + i.toString(), {
                    $domElement: $(el),
                    onChange: value => {

                        this._labels = this.$domElement.find('.decision-switch[data-val="yes"]').map((i, el) => {
                            return $(el).attr("data-label");
                        }).toArray();

                        const SELECTED = this.$domElement.find('.decision-switch[data-val="yes"]').map((i, el) => {
                            return $(el).attr("data-path");
                        }).toArray();

                        const ALL = this.$domElement.find('.decision-switch').filter((i, el) => {
                            return $(el).attr("data-val");
                        });

                        this.$btnNext[ALL.length == this.$switch.length ? "removeClass" : "addClass"]("disabled");

                        this._onSurveyChanged(SELECTED);

                        this.options.wheel.pZeroType = SELECTED.join().match("color") !== null;
                    }
                }));
            });

            this.$btnNext = this.$domElement.find(".btn-next");
            this.$btnNext.on("click", event => {
                if (this._labels) {
                    Globals.setUserData("survey", this._labels);
                }
            });
        }

        init() {
            super.init();
        }

        show(animated = true) {
            let nestedTL = new window.TimelineMax({ paused: true });

            nestedTL.set(this.$header, { yPercent: 0 });
            nestedTL.fromTo(this.$bg, 1.2, { opacity: 0 }, { force3D: "auto", opacity: 1, ease: window.Cubic.easeOut });
            nestedTL.addLabel("questions");
            nestedTL.staggerFromTo([this.$question1, this.$question2, this.$question3], 1, { y: 50, opacity: 0 }, { force3D: "auto", y: 0, opacity: 1, ease: window.Cubic.easeOut }, 0.15, "questions");
            nestedTL.staggerFromTo([this.$question1.find(".decision-switch"), this.$question2.find(".decision-switch"), this.$question3.find(".decision-switch")], 0.5, { y: -25, opacity: 0 }, { force3D: "auto", y: 0, opacity: 1, ease: window.Circ.easeOut }, 0.15, "questions+=0.4");
            nestedTL.fromTo(this.$footer, 0.85, { yPercent: 110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Cubic.easeOut }, "-=0.5");
            super.show(animated, nestedTL);
        }

        hide(animated = true) {
            let nestedTL = new window.TimelineMax({ paused: true });
            nestedTL.to(this.$footer, 0.65, { force3D: "auto", yPercent: 110, opacity: 0, ease: window.Expo.easeInOut });
            nestedTL.to(this.$header, 0.65, { force3D: "auto", yPercent: -110, opacity: 0, ease: window.Expo.easeInOut }, 0);

            const SURVEY = Globals.getUserData("survey").join();

            if (!SURVEY.length || SURVEY == "drive-competitions") {
                nestedTL.to(this.$wheelContainer, 0.65, { force3D: "auto", opacity: 0, ease: window.Expo.easeInOut }, 0);
            }

            nestedTL.staggerTo([this.$question3, this.$question2, this.$question1], 0.85, { force3D: "auto", y: 30, opacity: 0, ease: window.Expo.easeInOut }, 0.1, 0);
            nestedTL.to(this.$bg, 1.2, { force3D: "auto", opacity: 0, ease: window.Cubic.easeOut });
            super.hide(animated, nestedTL);
        }

        dispose() {
            this.$wheelContainer = null;
            this.$header = null;
            this.$footer = null;
            this.$questions = null;
            this.$question1 = null;
            this.$question2 = null;
            this.$question3 = null;
            this.$bg = null;
            this.$switch = null;
            this.$btnNext.off("click");
            this.$btnNext = null;
            super.dispose();
        }

        _onSurveyChanged(path) {

            Globals.SURVEY_PATH = path;

            if (Globals.SURVEY_PATH == Globals.COLOR_VIEW) {
                this.$domElement.find(".btn-next").attr("href", "#" + Globals.COLOR_VIEW);
            } else if (Globals.SURVEY_PATH == Globals.CONNESSO_VIEW) {
                this.$domElement.find(".btn-next").attr("href", "#" + Globals.CONNESSO_VIEW);
            } else if (Globals.SURVEY_PATH == Globals.PZERO_VIEW) {
                this.$domElement.find(".btn-next").attr("href", "#" + Globals.PZERO_VIEW);
            } else if (Globals.SURVEY_PATH == [Globals.COLOR_VIEW, Globals.CONNESSO_VIEW].join()) {
                this.$domElement.find(".btn-next").attr("href", "#" + Globals.COLOR_VIEW);
            } else if (Globals.SURVEY_PATH == [Globals.CONNESSO_VIEW, Globals.PZERO_VIEW].join()) {
                this.$domElement.find(".btn-next").attr("href", "#" + Globals.CONNESSO_VIEW);
            } else if (Globals.SURVEY_PATH == [Globals.COLOR_VIEW, Globals.PZERO_VIEW].join()) {
                this.$domElement.find(".btn-next").attr("href", "#" + Globals.COLOR_VIEW);
            } else if (Globals.SURVEY_PATH.match([Globals.COLOR_VIEW, Globals.CONNESSO_VIEW, Globals.PZERO_VIEW].join())) {
                this.$domElement.find(".btn-next").attr("href", "#" + Globals.COLOR_VIEW);
            } else {
                this.$domElement.find(".btn-next").attr("href", "#" + Globals.SERVICES_VIEW);
            }
        }
    }

    window.SurveySetupView = SurveySetupView;
})(window);
