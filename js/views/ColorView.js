(function (window) {

    class ColorView extends window.AbstractViewTimeline {

        static get TEMPLATE() {

            // Reset

            let next_href = "";

            if (Globals.SURVEY_PATH == Globals.COLOR_VIEW) {
                next_href = Globals.SERVICES_VIEW;
            } else if (Globals.SURVEY_PATH == [Globals.COLOR_VIEW, Globals.CONNESSO_VIEW].join()) {
                next_href = Globals.CONNESSO_VIEW;
            } else if (Globals.SURVEY_PATH == [Globals.COLOR_VIEW, Globals.PZERO_VIEW].join()) {
                next_href = Globals.PZERO_VIEW;
            } else if (Globals.SURVEY_PATH.match([Globals.COLOR_VIEW, Globals.CONNESSO_VIEW, Globals.PZERO_VIEW].join())) {
                next_href = Globals.CONNESSO_VIEW;
            }

            const TPL = $("#tpl__color-view").text();
            const COMPILED = _.template(TPL);
            return COMPILED({
                general: Globals.BRAND_DATA.general,
                username: Globals.currentUser.first_name + " " + Globals.currentUser.last_name,
                next_href: "#" + next_href
            });
        }

        constructor(id, options) {

            super(id, options);

            this.customColorsList = null;
            this.$wheelBg = $("#pirelli-wheel__bg");
            this.$standardColorsBtn = this.$domElement.find(".view-content .view-main #color-view__standard-colors-btn");
            this.$customiseColorsBtn = this.$domElement.find(".view-content .view-main #color-view__customise-colors-btn");

            this.$wheelContainer = $("#pirelli-wheel-container");
            this.$wheel = this.$wheelContainer.find("#pirelli-wheel");
            this.$wheelBorder = this.$wheelContainer.find("#pirelli-wheel__border");
            this.$colorsStandardTriggers = this.$wheelBorder.find(".pirelli-wheel__colors .pirelli-wheel__border-trigger");
            this.$wheelBorderSvgCircle1 = this.$wheelBorder.find("> svg circle:first-of-type");
            this.$wheelBorderSvgCircle2 = this.$wheelBorder.find("> svg circle:last-of-type");
            this.$dragger = $("#dragger");
            this.$header = this.$domElement.find(".view-header");
            this.$footer = this.$domElement.find(".view-footer");

            this.$customColor = this.$domElement.find("#color-view__selected-custom-color");
            this.$customColorLabel = this.$customColor.find("#color-view__selected-custom-color-code");

            this.AJAXrequest = null;

            this.$standardColorsBtn.prop("disabled", true);
            this.$customiseColorsBtn.prop("disabled", false);
        }

        init() {

            /* Add Modules to the modules list */

            /* super.addModules(); */

            /* */

            this.$btnNext = this.$domElement.find(".btn-next");
            this.$btnNext.on("click", event => {
                if (this.options.wheel && this.options.wheel.currentColorSelection) {
                    Globals.setUserData("color", this.options.wheel.currentColorSelection);
                }
            });

            /* Call super init */

            super.init();

            /* Overlay */

            this.overlay = new window.ColorViewOverlay(this.id + "__overlay", {
                $domElement: this.$domElement.find(".view-overlay")
            });

            this.overlay.init();

            this.$colorViewPickerGridContainer = this.$domElement.find("#color-view__picker-grid-container");
            this.$colorViewPickerWrapper = this.$domElement.find("#color-view__picker-wrapper");
            this.$colorViewPickerGrid = this.$colorViewPickerWrapper.find("#color-view__picker-grid");

            this.drg = Draggable.create(this.$colorViewPickerGrid, {
                type: "y",
                edgeResistance: 0.5,
                throwProps: true,
                bounds: this.$colorViewPickerGridContainer
            });

            this.AJAXrequest = $.ajax({
                dataType: "json",
                type: "GET",
                url: "resources/data/custom-colors.json",
                success: data => {
                    this._createColorPalette(data.colors);
                    this.$colorsBtn = this.$colorViewPickerGrid.find("[data-code]");
                    this.$colorsBtn.off().on("click", event => {
                        event.stopPropagation();
                        event.preventDefault();

                        window.TweenMax.to(this.$customColorLabel, 1, { scrambleText: { text: $(event.delegateTarget).attr("data-code"), speed: 1, ease: Linear.easeNone } });

                        if (this.$customColor.find("span:nth-child(1)").css("opacity") == 0) {
                            window.TweenMax.staggerFromTo(this.$customColor.find("span"), 0.8, {
                                cycle: {
                                    x: [-15, 15]
                                },
                                opacity: 0
                            }, {
                                force3D: "auto",
                                x: 0,
                                opacity: 1,
                                ease: window.Expo.easeInOut
                            }, 0);
                        }

                        this.onCustomColorSelect($(event.delegateTarget).css("background-color"), $(event.delegateTarget).attr("data-code"));
                    });

                    this.$standardColorsBtn.on("click", () => {
                        this.$standardColorsBtn.prop("disabled", true);
                        this.$customiseColorsBtn.prop("disabled", false);
                        this.overlay.hide();

                        let tl = new window.TimelineMax({
                            onComplete: () => {
                                this.options.wheel.customColors = false;
                            }
                        });

                        tl.staggerTo(this.$customColor.find("span"), 0.8, { force3D: "auto", cycle: { x: [-15, 15] }, opacity: 0, ease: window.Expo.easeInOut }, 0);
                    });

                    this.$customiseColorsBtn.on("click", () => {
                        this.$standardColorsBtn.prop("disabled", false);
                        this.$customiseColorsBtn.prop("disabled", true);
                        this.overlay.show();
                        this.options.wheel.customColors = true;
                    });
                },
                error: err => {}
            });

            TweenMax.set(this.$customColor.find("span:nth-child(1)"), { x: -15, opacity: 0 });
            TweenMax.set(this.$customColor.find("span:nth-child(2)"), { x: 15, opacity: 0 });
        }

        show(animated = true) {
            let nestedTL = new window.TimelineMax({
                paused: true
            });
            nestedTL.fromTo(this.$header, 0.8, { yPercent: -110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Expo.easeInOut });
            nestedTL.fromTo(this.$dragger, 0.8, { opacity: 0 }, { force3D: "auto", opacity: 1, ease: window.Expo.easeInOut }, "-=0.5");
            nestedTL.addLabel("step1", "-=0.8");
            nestedTL.fromTo([this.$wheelBorderSvgCircle1, this.$wheelBorderSvgCircle2], 1, { transformOrigin: "center", scale: 0.75 }, { force3D: "auto", scale: 1, ease: window.Expo.easeInOut }, "step1");
            nestedTL.staggerFromTo(this.$colorsStandardTriggers, 1, { opacity: 0 }, { force3D: "auto", opacity: 1, ease: window.Expo.easeInOut }, 0.075, "step1+=0.3");
            nestedTL.staggerFromTo([this.$standardColorsBtn.parent(), this.$customiseColorsBtn.parent()], 0.8, { opacity: 0, cycle: { y: [50, -50] } }, { force3D: "auto", opacity: 1, y: 0, ease: window.Expo.easeInOut }, 0, "step1+=0.8");
            nestedTL.fromTo(this.$domElement.find("#color-view__shop-cta"), 0.65, { x: -300 }, { force3D: "auto", x: 0, ease: window.Cubic.easeOut }, "step1+=1.3");
            nestedTL.fromTo(this.$footer, 0.85, { yPercent: 110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Cubic.easeOut }, "-=0.3");
            super.show(animated, nestedTL);
        }

        hide(animated = true) {

            this.overlay.hide(animated);

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.to(this.$footer, 0.65, { force3D: "auto", yPercent: 110, opacity: 0, ease: window.Expo.easeInOut }, 0);
            nestedTL.to(this.$header, 0.65, { force3D: "auto", yPercent: -110, opacity: 0, ease: window.Expo.easeInOut }, 0);
            nestedTL.to(this.$domElement.find("#color-view__shop-cta"), 0.65, { force3D: "auto", x: -300, ease: window.Expo.easeInOut }, 0.15);

            if (!this.options.wheel.customColors) {
                nestedTL.addLabel("wheel");
                nestedTL.staggerTo(this.$colorsStandardTriggers, 0.65, { force3D: "auto", scale: 0, opacity: 0, ease: window.Expo.easeInOut }, 0.1, 0);
                nestedTL.staggerTo([this.$standardColorsBtn.parent(), this.$customiseColorsBtn.parent()], 1.2, { force3D: "auto", opacity: 0, cycle: { y: [-50, 50] }, ease: window.Expo.easeInOut }, 0, "wheel");
                nestedTL.to(this.$dragger, 0.8, { force3D: "auto", opacity: 0, ease: window.Expo.easeInOut }, "wheel-=0.55");
                nestedTL.to([this.$wheelBorderSvgCircle1, this.$wheelBorderSvgCircle2], 0.65, { transformOrigin: "center", force3D: "auto", opacity: 0, scale: 0.75, ease: window.Expo.easeInOut }, "wheel-=0.3");
            } else {
                nestedTL.addLabel("wheel", "-=0.4");
                nestedTL.staggerTo(this.$customColor.find("span"), 0.65, { cycle: { x: [-15, 15] }, opacity: 0, ease: window.Expo.easeOut }, 0, 0);
            }

            const SURVEY = Globals.getUserData("survey").join();
            if (!SURVEY.match("new-technologies")) {
                nestedTL.to(this.$wheelContainer, 0.65, { force3D: "auto", opacity: 0, ease: window.Expo.easeInOut });
            } else {
                nestedTL.to(this.$wheelBg, 0.65, { force3D: "auto", opacity: 0, ease: window.Expo.easeInOut }, "wheel");
            }

            super.hide(animated, nestedTL);
        }

        dispose() {

            this.$btnNext.off();

            if (this.drg) {
                this.drg[0].disable();
            }

            this.$colorsBtn.off();

            if (this.AJAXrequest) {
                this.AJAXrequest.abort();
                this.AJAXrequest = null;
            }

            this.$wheelBg = null;
            this.$wheelContainer = null;
            this.$wheel = null;
            this.$colorsStandardTriggers = null;
            this.$wheelBorder = null;
            this.$wheelBorderSvgCircle1 = null;
            this.$wheelBorderSvgCircle2 = null;
            this.$header = null;
            this.$footer = null;
            this.$dragger = null;
            this.$standardColorsBtn.off("click");
            this.$standardColorsBtn = null;

            this.$customiseColorsBtn.off("click");
            this.$customiseColorsBtn = null;

            this.customColorsList = null;

            this.overlay.dispose();
            this.overlay = null;

            super.dispose();
        }

        _createColorPalette(colors) {
            for (var i = 0; i < colors.length; i++) {
                this.$colorViewPickerGrid.append(`<div data-code="${colors[i].code}" style="background-color:${colors[i].hex}"></div>`);
            }
        }
    }

    window.ColorView = ColorView;
})(window);
