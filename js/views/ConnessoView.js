(function (window) {

    class ConnessoView extends window.AbstractViewTimeline {

        static get TEMPLATE() {

            let next_href = "";

            if (Globals.SURVEY_PATH == Globals.CONNESSO_VIEW) {
                next_href = Globals.SERVICES_VIEW;
            } else if (Globals.SURVEY_PATH == [Globals.COLOR_VIEW, Globals.CONNESSO_VIEW].join()) {
                next_href = Globals.SERVICES_VIEW;
            } else if (Globals.SURVEY_PATH == [Globals.CONNESSO_VIEW, Globals.PZERO_VIEW].join()) {
                next_href = Globals.PZERO_VIEW;
            } else if (Globals.SURVEY_PATH.match([Globals.COLOR_VIEW, Globals.CONNESSO_VIEW, Globals.PZERO_VIEW].join())) {
                next_href = Globals.PZERO_VIEW;
            }

            const TPL = $("#tpl__connesso-view").text();
            const COMPILED = _.template(TPL);
            return COMPILED({
                username: Globals.currentUser.first_name + " " + Globals.currentUser.last_name,
                vehicle_brand: Globals.currentUser.vehicle_brand,
                next_href: "#" + next_href
            });
        }

        constructor(id, options) {

            super(id, options);

            this.$connessoViewWheelCloneWrapper = this.$domElement.find("#connesso-view__wheel-clone-wrapper");

            this.$connessoViewWheelCloneWrapper.html($("#pirelli-wheel-container #pirelli-wheel-grpx").clone());

            if (Globals.getUserData("color")) {
                this.$connessoViewWheelCloneWrapper.find("#pirelli-wheel-pzero-default").remove();
                this.$connessoViewWheelCloneWrapper.find("#pirelli-wheel-pzero-customizable").css("opacity", 1);
            } else {
                this.$connessoViewWheelCloneWrapper.find("#pirelli-wheel-pzero-default").css("opacity", 1);
                this.$connessoViewWheelCloneWrapper.find("#pirelli-wheel-pzero-customizable").remove();
            }

            this.$clonedWheelParent = this.$domElement.find("#connesso-view__wheel");
            this.$clonedWheel = this.$clonedWheelParent.find("#connesso-view__wheel-clone-wrapper");

            this.wheelLoopTL = null;
            this.$infoText = this.$domElement.find("#connesso-view__info-text");

            this.$header = this.$domElement.find(".view-header");
            this.$footer = this.$domElement.find(".view-footer");

            this.$ringsWrapper = this.$domElement.find("#connesso-view__wheel-rings");
            this.$ringSM = this.$domElement.find("#connesso-view__wheel-sm-ring");
            this.$ringMD = this.$domElement.find("#connesso-view__wheel-md-ring");
            this.$ringLG = this.$domElement.find("#connesso-view__wheel-lg-ring");

            this.$bgImg = this.$domElement.find(".view-bg .view-bg-img");
            this.$particlesBackContainer = this.$domElement.find("#connesso-view__back-particles-container");
            this.$particlesMidContainer = this.$domElement.find("#connesso-view__mid-particles-container");
            this.$particlesFrontContainer = this.$domElement.find("#connesso-view__front-particles-container");
        }

        init() {

            this.$domElement.one(AbstractElement.EL_SHOW_START, event => {
                event.stopPropagation();
                event.preventDefault();
                this._startParticles();
            });

            super.init();

            /* Overlay */

            this.overlay = new window.ConnessoViewOverlay(this.id + "__overlay", {
                $domElement: this.$domElement.find(".view-overlay")
            });

            this.overlay.init();

            this.$playVideoBtn = this.$domElement.find("#connesso-view__play-video");

            this.$playVideoBtn.on("click", event => {
                this.overlay.show();
            });

            this.overlay.$domElement.on(AbstractElement.EL_SHOW_COMPLETE, event => {
                this._stopParticles();
            });

            this.overlay.$domElement.on(AbstractElement.EL_HIDE_START, event => {
                this._startParticles();
            });
        }

        show(animated = true) {
            let nestedTL = new window.TimelineMax({ paused: true });

            nestedTL.set(this.$clonedWheelParent, { yPercent: 0 });
            nestedTL.set(this.$ringsWrapper, { opacity: 0 });
            nestedTL.fromTo(this.$header, 0.8, { yPercent: -110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Expo.easeInOut });
            //nestedTL.fromTo(this.$bgImg, 0.85, { opacity: 0}, {opacity: 1, ease: window.Cubic.easeOut}, "-=0.5");
            nestedTL.addLabel("fx", "-=0.2");
            nestedTL.to(window, 0, { onStart: this._startWaves, onStartScope: this }, "fx");
            nestedTL.set(this.$ringsWrapper, { opacity: 1 }, "fx");
            nestedTL.staggerFromTo([this.$particlesBackContainer, this.$particlesFrontContainer, this.$particlesMidContainer], 0.85, { opacity: 0 }, { force3D: "auto", opacity: 1, ease: window.Cubic.easeOut }, 0.2, "fx");
            nestedTL.staggerFromTo([this.$infoText, this.$playVideoBtn], 0.85, { opacity: 0, cycle: { y: [50, -50] } }, { force3D: "auto", y: 0, opacity: 1, ease: window.Cubic.easeOut }, 0, "-=0.5");
            nestedTL.fromTo(this.$footer, 0.85, { yPercent: 110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Cubic.easeOut }, "+=0.5");
            super.show(animated, nestedTL);
        }

        hide(animated = true) {

            let nestedTL = new window.TimelineMax({ paused: true });
            nestedTL.to(this.$footer, 0.65, { force3D: "auto", yPercent: 110, opacity: 0, ease: window.Expo.easeInOut });
            nestedTL.to(this.$header, 0.65, { force3D: "auto", yPercent: -110, opacity: 0, ease: window.Expo.easeInOut }, 0);
            nestedTL.staggerTo([this.$infoText, this.$playVideoBtn], 0.85, { force3D: "auto", cycle: { y: [-50, 50] }, opacity: 0, ease: window.Expo.easeInOut }, 0, 0);
            nestedTL.addLabel("fx", "-=0.5");
            nestedTL.to(this.$ringsWrapper, 0.65, { force3D: "auto", opacity: 0, ease: window.Cubic.easeOut }, "fx");
            nestedTL.staggerTo([this.$particlesFrontContainer, this.$particlesBackContainer, this.$particlesMidContainer], 0.85, { force3D: "auto", opacity: 0, ease: window.Expo.easeInOut, onComplete: () => {
                    this._stopParticles();
                } }, 0.1, "fx");
            nestedTL.to(this.$clonedWheelParent, 1.2, { yPercent: -110, ease: window.Expo.easeInOut }, "-=0.8");
            nestedTL.to(this.$bgImg, 0.65, { opacity: 0, ease: window.Expo.easeInOut });
            super.hide(animated, nestedTL);
        }

        onHideComplete() {
            if (this.wheelLoopTL) {
                this.wheelLoopTL.kill();
                this.wheelLoopTL = null;
            }
        }

        _startParticles() {

            window.particlesJS('connesso-view__back-particles-container', {
                "particles": {
                    "number": {
                        "value": 30,
                        "density": {
                            "enable": true,
                            "value_area": 900
                        }
                    },
                    "color": {
                        "value": "#249494"
                    },
                    "shape": {
                        "type": "circle",
                        "stroke": {
                            "width": 0,
                            "color": "#249494"
                        },
                        "polygon": {
                            "nb_sides": 5
                        },
                        "image": {
                            "src": "img/github.svg",
                            "width": 100,
                            "height": 100
                        }
                    },
                    "opacity": {
                        "value": 0.5,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 1,
                            "opacity_min": 0.1,
                            "sync": false
                        }
                    },
                    "size": {
                        "value": 12,
                        "random": false,
                        "anim": {
                            "enable": false,
                            "speed": 40,
                            "size_min": 0.1,
                            "sync": false
                        }
                    },
                    "line_linked": {
                        "enable": true,
                        "distance": 150,
                        "color": "#6cfcfc",
                        "opacity": 0.4,
                        "width": 4
                    },
                    "move": {
                        "enable": true,
                        "speed": 1.2,
                        "direction": "top-right",
                        "random": false,
                        "straight": false,
                        "out_mode": "out",
                        "attract": {
                            "enable": false,
                            "rotateX": 600,
                            "rotateY": 1200
                        }
                    }
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                        "onhover": {
                            "enable": false,
                            "mode": "repulse"
                        },
                        "onclick": {
                            "enable": true,
                            "mode": "push"
                        },
                        "resize": true
                    },
                    "modes": {
                        "grab": {
                            "distance": 400,
                            "line_linked": {
                                "opacity": 1
                            }
                        },
                        "bubble": {
                            "distance": 400,
                            "size": 40,
                            "duration": 2,
                            "opacity": 8,
                            "speed": 3
                        },
                        "repulse": {
                            "distance": 200
                        },
                        "push": {
                            "particles_nb": 4
                        },
                        "remove": {
                            "particles_nb": 2
                        }
                    }
                },
                "retina_detect": true,
                "config_demo": {
                    "hide_card": false,
                    "background_color": "#b61924",
                    "background_image": "",
                    "background_position": "50% 50%",
                    "background_repeat": "no-repeat",
                    "background_size": "cover"
                }
            });

            window.particlesJS('connesso-view__mid-particles-container', {
                "particles": {
                    "number": {
                        "value": 30,
                        "density": {
                            "enable": true,
                            "value_area": 1000
                        }
                    },
                    "color": {
                        "value": "#238c8c"
                    },
                    "shape": {
                        "type": "circle",
                        "stroke": {
                            "width": 0,
                            "color": "#238c8c"
                        },
                        "polygon": {
                            "nb_sides": 5
                        },
                        "image": {
                            "src": "img/github.svg",
                            "width": 100,
                            "height": 100
                        }
                    },
                    "opacity": {
                        "value": 0.5,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 1,
                            "opacity_min": 0.1,
                            "sync": false
                        }
                    },
                    "size": {
                        "value": 4,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 40,
                            "size_min": 0.1,
                            "sync": false
                        }
                    },
                    "line_linked": {
                        "enable": false,
                        "distance": 130,
                        "color": "#238c8c",
                        "opacity": 0.3,
                        "width": 2
                    },
                    "move": {
                        "enable": true,
                        "speed": 1.8,
                        "direction": "top-right",
                        "random": false,
                        "straight": false,
                        "out_mode": "out",
                        "attract": {
                            "enable": false,
                            "rotateX": 600,
                            "rotateY": 1200
                        }
                    }
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                        "onhover": {
                            "enable": false,
                            "mode": "repulse"
                        },
                        "onclick": {
                            "enable": true,
                            "mode": "push"
                        },
                        "resize": true
                    },
                    "modes": {
                        "grab": {
                            "distance": 400,
                            "line_linked": {
                                "opacity": 1
                            }
                        },
                        "bubble": {
                            "distance": 400,
                            "size": 40,
                            "duration": 2,
                            "opacity": 8,
                            "speed": 3
                        },
                        "repulse": {
                            "distance": 200
                        },
                        "push": {
                            "particles_nb": 4
                        },
                        "remove": {
                            "particles_nb": 2
                        }
                    }
                },
                "retina_detect": true,
                "config_demo": {
                    "hide_card": false,
                    "background_color": "#b61924",
                    "background_image": "",
                    "background_position": "50% 50%",
                    "background_repeat": "no-repeat",
                    "background_size": "cover"
                }
            });

            window.particlesJS('connesso-view__front-particles-container', {
                "particles": {
                    "number": {
                        "value": 40,
                        "density": {
                            "enable": true,
                            "value_area": 350
                        }
                    },
                    "color": {
                        "value": "#37e0e0"
                    },
                    "shape": {
                        "type": "circle",
                        "stroke": {
                            "width": 0,
                            "color": "#37e0e0"
                        },
                        "polygon": {
                            "nb_sides": 5
                        },
                        "image": {
                            "src": "img/github.svg",
                            "width": 100,
                            "height": 100
                        }
                    },
                    "opacity": {
                        "value": 0.7,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 1,
                            "opacity_min": 0.1,
                            "sync": false
                        }
                    },
                    "size": {
                        "value": 3,
                        "random": false,
                        "anim": {
                            "enable": false,
                            "speed": 40,
                            "size_min": 0.1,
                            "sync": false
                        }
                    },
                    "line_linked": {
                        "enable": true,
                        "distance": 130,
                        "color": "#37e0e0",
                        "opacity": 0.3,
                        "width": 2
                    },
                    "move": {
                        "enable": true,
                        "speed": 8,
                        "direction": "top-right",
                        "random": false,
                        "straight": false,
                        "out_mode": "out",
                        "attract": {
                            "enable": false,
                            "rotateX": 600,
                            "rotateY": 1200
                        }
                    }
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                        "onhover": {
                            "enable": false,
                            "mode": "repulse"
                        },
                        "onclick": {
                            "enable": false,
                            "mode": "push"
                        },
                        "resize": true
                    },
                    "modes": {
                        "grab": {
                            "distance": 400,
                            "line_linked": {
                                "opacity": 1
                            }
                        },
                        "bubble": {
                            "distance": 400,
                            "size": 40,
                            "duration": 2,
                            "opacity": 8,
                            "speed": 3
                        },
                        "repulse": {
                            "distance": 200
                        },
                        "push": {
                            "particles_nb": 4
                        },
                        "remove": {
                            "particles_nb": 2
                        }
                    }
                },
                "retina_detect": true,
                "config_demo": {
                    "hide_card": false,
                    "background_color": "#b61924",
                    "background_image": "",
                    "background_position": "50% 50%",
                    "background_repeat": "no-repeat",
                    "background_size": "cover"
                }
            });
        }

        _stopParticles() {
            if (window.pJSDom[1]) {
                window.pJSDom[1].pJS.fn.vendors.destroypJSAtIndex(1);
            }

            if (window.pJSDom[0]) {
                window.pJSDom[0].pJS.fn.vendors.destroypJSAtIndex(0);
            }
        }

        _startWaves() {
            this.wheelLoopTL = new window.TimelineMax({ repeat: -1 });
            this.wheelLoopTL.set([this.$ringSM, this.$ringMD, this.$ringLG], { opacity: 0 });
            this.wheelLoopTL.fromTo(this.$ringLG, 1.7, { transformOrigin: "center", opacity: 1, scale: 1.2, y: 50 }, { force3D: "auto", opacity: 0, scale: 1, y: 0, ease: window.Circ.easeOut }, 0);
            this.wheelLoopTL.fromTo(this.$ringMD, 1.5, { transformOrigin: "center", opacity: 1, scale: 1.3, y: 50 }, { force3D: "auto", opacity: 0, scale: 0.75, y: 0, ease: window.Circ.easeOut }, 0);
            this.wheelLoopTL.fromTo(this.$ringSM, 1.2, { transformOrigin: "center", opacity: 1.2, scale: 1.6, y: 50 }, { force3D: "auto", opacity: 0, scale: 0.5, y: 0, ease: window.Circ.easeOut }, 0);
        }

        dispose() {
            this._stopParticles();

            this.overlay.dispose();
            this.overlay = null;

            this.$playVideoBtn.off("click");
            this.$playVideoBtn = null;

            this.$header = null;
            this.$footer = null;

            this.$ringsWrapper = null;
            this.$ringSM = null;
            this.$ringMD = null;
            this.$ringLG = null;

            this.$infoText = null;
            this.$clonedWheel = null;
            this.$connessoViewWheelCloneWrapper.empty();
            this.$connessoViewWheelCloneWrapper = null;

            this.$particlesBackContainer = null;
            this.$particlesMidContainer = null;
            this.$particlesFrontContainer = null;

            super.dispose();
        }
    }

    window.ConnessoView = ConnessoView;
})(window);
