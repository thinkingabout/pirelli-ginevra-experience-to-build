(function (window) {

    class WelcomeView extends window.AbstractViewTimeline {

        static get TEMPLATE() {
            const TPL = $("#tpl__welcome-view").text();
            const COMPILED = _.template(TPL);
            return COMPILED({
                general: Globals.BRAND_DATA.general,
                username: Globals.currentUser.first_name + " " + Globals.currentUser.last_name
            });
        }

        constructor(id, options) {
            super(id, options);

            this.$bg = this.$domElement.find(".view-bg");
            this.$bgImg = this.$bg.find(".view-bg-img");
            this.$header = this.$domElement.find(".view-header");
            this.$brandLogo = this.$domElement.find("#welcome-view__brand-logo");
            this.$welcomeLabel = this.$domElement.find("#welcome-view__welcome-label");
            this.$username = this.$domElement.find("#welcome-view__username");
            this.$footer = this.$domElement.find(".view-footer");

            this.$username.html(Globals.currentUser.first_name + " " + Globals.currentUser.last_name);

            /**
            *   Reset user if already done
            */

            Globals.resetStorageUserData();
        }

        show(isAnimated = true) {
            let userSplitText = new window.SplitText(this.$username.get(0), {
                type: "words, chars"
            });

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.fromTo(this.$bgImg, 1.5, { yPercent: -110 }, { force3D: "auto", yPercent: 0, ease: window.Expo.easeInOut }, 0.55);
            nestedTL.fromTo(this.$brandLogo, 0.65, { scale: 0.45, opacity: 0 }, { force3D: "auto", transformOrigin: "center bottom", scale: 1, opacity: 1, ease: window.Cubic.easeOut }, "-=0.4");
            nestedTL.fromTo(this.$welcomeLabel, 0.65, { y: -30, opacity: 0 }, { force3D: "auto", y: 0, opacity: 1, ease: window.Cubic.easeOut }, "-=0.25");
            nestedTL.staggerFromTo(userSplitText.chars, 0.65, { opacity: 0, scale: 0, y: 20, rotationX: -180, transformOrigin: "0% 50% -50" }, { force3D: "auto", opacity: 1, scale: 1, y: 0, rotationX: 0, ease: window.Cubic.easeOut }, 0.0125, "-=0.65");
            nestedTL.fromTo(this.$header, 0.55, { scale: 0.75, y: -30, opacity: 0 }, { force3D: "auto", scale: 1, y: 0, opacity: 1, ease: window.Cubic.easeOut }, "-=0.3");
            nestedTL.fromTo(this.$footer, 0.85, { yPercent: 110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Cubic.easeOut });

            super.show(isAnimated, nestedTL);
        }

        hide(isAnimated = true) {

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.addLabel("texts");
            nestedTL.to(this.$footer.find(".btn-next > *:not(.v-line)"), 0.45, { force3D: "auto", y: 5, opacity: 0, ease: window.Expo.easeInOut }, "texts");
            nestedTL.to(this.$welcomeLabel, 0.65, { force3D: "auto", y: -30, opacity: 0, ease: window.Expo.easeInOut }, "texts");
            nestedTL.to(this.$username, 0.65, { force3D: "auto", opacity: 0, y: 20, ease: window.Expo.easeInOut }, "texts");
            nestedTL.to(this.$header, 0.75, { force3D: "auto", scale: 0.75, y: -200, opacity: 0, ease: window.Expo.easeInOut }, "texts+=0.2");
            nestedTL.to(this.$brandLogo, 0.75, { force3D: "auto", transformOrigin: "center bottom", scale: 0.45, opacity: 0, ease: window.Expo.easeInOut }, "texts+=0.3");
            nestedTL.to(this.$bgImg, 1.2, { force3D: "auto", yPercent: -110, opacity: 0, ease: window.Expo.easeInOut }, "texts+=0.5");
            super.hide(isAnimated, nestedTL);
        }

        dispose() {
            //this.$wheelContainer = null;
            //this.$wheelBg = null;
            this.$bg = null;
            this.$bgImg = null;
            this.$header = null;
            this.$brandLogo = null;
            this.$welcomeLabel = null;
            this.$username = null;
            this.$footer = null;
            this.$wheelBg = null;

            super.dispose();
        }

    }

    window.WelcomeView = WelcomeView;
})(window);
