(function (window) {

    class TechnologyView extends window.AbstractViewTimeline {

        static get TEMPLATE() {
            const TPL = $("#tpl__technology-view").text();
            const COMPILED = _.template(TPL);
            return COMPILED({
                username: Globals.currentUser.first_name + " " + Globals.currentUser.last_name,
                general: Globals.BRAND_DATA.general,
                data: Globals.BRAND_DATA.view.technology
            });
        }

        /* Submodules IDs */

        constructor(id, options) {

            super(id, options);

            this.$wheelContainer = $("#pirelli-wheel-container");
            this.$technology = this.$wheelContainer.find(".pirelli-wheel__technology");
            this.$technologyCuePoints = this.$technology.find(".pirelli-wheel__cue-point-container");
            this.$header = this.$domElement.find(".view-header");
            this.$footer = this.$domElement.find(".view-footer");

            if (Globals.BRAND_DATA.view.technology.table && Globals.BRAND_DATA.view.technology.table.length) this.$tableBtn = this.$domElement.find("#technology-view__table-btn");
            this.carouselOverlay = null;
            this.tableOverlay = null;
        }

        init() {

            /* Call super init */

            super.init();

            /* */
            this.carouselOverlay = new window.TechnologyViewCarouselOverlay(this.id + "__carousel-overlay", {
                $domElement: this.$domElement.find("#technology-view-carousel-overlay.view-overlay")
            });

            this.carouselOverlay.init();

            /* Listen to the selection of the trigger points */

            this.options.wheel.onTechnologyPointSelected = () => {
                this.carouselOverlay.show();
            };

            if (Globals.BRAND_DATA.view.technology.table && Globals.BRAND_DATA.view.technology.table.length) {
                this.tableOverlay = new window.TechnologyViewTableOverlay(this.id + "__table-overlay", {
                    $domElement: this.$domElement.find("#technology-view-table-overlay.view-overlay")
                });

                this.tableOverlay.init();

                this.$tableBtn.on("click", event => {
                    this.tableOverlay.show();
                });
            }
        }

        show(animated = true) {
            let nestedTL = new window.TimelineMax({ paused: true });
            nestedTL.addLabel("step0", "+=0.6");
            nestedTL.fromTo(this.$header, 1.5, { y: $(window).height() * 2 }, { force3D: "auto", y: 0, ease: window.Expo.easeInOut }, "step0");
            nestedTL.fromTo(this.$header.find("h2"), 1.6, { transformOrigin: "center top", scale: 4, opacity: 0 }, { force3D: "auto", scale: 1, opacity: 1, ease: window.Circ.easeOut }, "step0");
            nestedTL.fromTo(this.$wheelContainer, 1, { transformOrigin: "center", scale: 2, yPercent: 150 }, { force3D: "auto", scale: 1, yPercent: 0, ease: window.Cubic.easeOut }, "step0+=0.65");
            nestedTL.addLabel("cuepoints", "-=0.5");
            nestedTL.fromTo(this.$technologyCuePoints.find(".pirelli-wheel__cue-point"), 0.7, { transformOrigin: "center", scale: 0 }, { scale: 1, ease: window.Back.easeOut }, "cuepoints");

            if (Globals.BRAND_DATA.view.technology.table && Globals.BRAND_DATA.view.technology.table.length) {
                nestedTL.fromTo(this.$tableBtn, 0.4, { x: 60, opacity: 0 }, { x: 0, opacity: 1, ease: window.Circ.easeOut }, "cuepoints+=0.3");
            }

            nestedTL.fromTo(this.$footer, 0.85, { yPercent: 110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Cubic.easeOut }, "cuepoints");

            super.show(animated, nestedTL);
        }

        hide(animated = true) {
            this.carouselOverlay.hide(animated);

            if (Globals.BRAND_DATA.view.technology.table && Globals.BRAND_DATA.view.technology.table.length) {
                this.tableOverlay.hide(animated);
            }

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.to(this.$footer, 0.5, { force3D: "auto", yPercent: 110, opacity: 0, ease: window.Expo.easeInOut }, 0);

            if (Globals.BRAND_DATA.view.technology.table && Globals.BRAND_DATA.view.technology.table.length) {
                nestedTL.to(this.$tableBtn, 0.8, { x: 60, opacity: 0, ease: window.Cubic.easeInOut }, 0.2);
            }

            nestedTL.to(this.$technologyCuePoints.find(".pirelli-wheel__cue-point"), 0.5, { transformOrigin: "center", scale: 0, ease: window.Back.easeIn }, 0.4);

            super.hide(animated, nestedTL);
        }

        dispose() {
            if (Globals.BRAND_DATA.view.technology.table && Globals.BRAND_DATA.view.technology.table.length) {
                this.$tableBtn.off();
                this.$tableBtn = null;

                this.tableOverlay.dispose();
                this.tableOverlay = null;
            }

            this.$wheelContainer = null;
            this.$technologyCuePoints = null;
            this.$header = null;
            this.$footer = null;
            this.carouselOverlay.dispose();
            this.carouselOverla = null;
            super.dispose();
        }
    }

    window.TechnologyView = TechnologyView;
})(window);
