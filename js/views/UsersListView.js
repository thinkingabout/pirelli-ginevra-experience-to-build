(function (window) {

    class UsersListView extends window.AbstractViewTimeline {

        static get TEMPLATE() {
            const TPL = $("#tpl__users-list-view").text();
            const COMPILED = _.template(TPL);
            return COMPILED();
        }

        constructor(id, options) {

            super(id, options);

            this.$newUserBtn = this.$domElement.find("#new-user-btn");
            this.$updateUsersBtn = this.$domElement.find("#update-users-btn");
            this.$listGroup = this.$domElement.find(".list-group");
            this.$sendUsersBtn = this.$domElement.find("#send-users-btn");
            this.$usersListItems = null;

            this.newUserModal = new window.NewUserModal(this._gotoWelcomeView.bind(this));
            this.registeredUserModal = new window.RegisteredUserModal(this._gotoWelcomeView.bind(this));
            this.listUpdateModal = new window.ListUpdateModal();
        }

        init() {
            this.newUserModal.init();
            this.registeredUserModal.init();
            this.listUpdateModal.init();

            super.init();

            this.$newUserBtn.on("click", event => {
                event.stopPropagation();

                this.newUserModal.show();
            });

            this.$updateUsersBtn.on("click", event => {
                event.stopPropagation();
                if ($(event.delegateTarget).hasClass("btn-danger")) return;

                this._retrieveUserData();
            });

            /*alert("CHECK CONNECTION: " + Globals.checkConnection());*/

            if (Globals.checkConnection()) {
                this.$updateUsersBtn.removeClass("btn-danger").addClass("btn-success");
            }

            document.addEventListener("online", event => {
                this.$updateUsersBtn.removeClass("btn-danger").addClass("btn-success");
            }, false);

            document.addEventListener("offline", event => {
                this.$updateUsersBtn.removeClass("btn-success").addClass("btn-danger");
            }, false);

            this._retrieveUserData();

            /**/

            this.$sendUsersBtn.on("click", event => {

                event.stopPropagation();
                event.preventDefault();

                const USER_DATA = Globals.getStorageData();

                if (!USER_DATA) {
                    alert("Data are not available.");
                }

                const SUBJECT = "Pirelli Ginevra Experience | Report [" + new Date().toLocaleString() + "]";
                const BODY = window.localStorage.user_data;
                let URI = "mailto:report.mugello@gmail.com?subject=";
                URI += encodeURIComponent(SUBJECT);
                URI += "&body=";
                URI += encodeURIComponent(BODY);
                URI += "&cc=thinkingaboutdev@gmail.com";
                document.location.href = URI;

                return false;
            });
        }

        _gotoWelcomeView(currentUserObj) {

            Globals.currentUser = currentUserObj;

            //

            let userInfo = _.clone(currentUserObj);
            delete userInfo.experienceDone; // revove this field from the report
            Globals.setUserData("user_info", userInfo);

            this.AJAXrequest = $.ajax({
                dataType: "json",
                type: "GET",
                url: window.AppStaticData.getBrand(Globals.currentUser.vehicle_brand).json,
                success: data => {
                    this._compileExperience(data);
                },
                error: err => {
                    alert("There was an error loading the brand data, please close & restart the app.");
                }
            });
        }

        _compileExperience(data) {

            /**
             * Brand Data
             */

            Globals.setBrandData(data);
            $("#pirelli-wheel-pzero-logo").css("background-image", "url(" + data.general.wheel_logo + ")");
            $("html").attr("data-vehicle-brand", data.general.brand.toLowerCase().replace(" ", "-"));
            $("html").attr("data-prestige", data.general.is_prestige);

            $("head #thankyou-view__injected-head-styles").text('#thankyou-view__pzero-color-wheel:after, #thankyou-view__connesso-wheel:after {background-image:url("' + data.general.wheel_logo + '")!important;}');

            /**
             * Start the experience
             */

            window.location.hash = "welcome";
        }

        _retrieveUserData() {
            if (Globals.checkConnection()) {
                this._fetchUser();
            } else {
                this._retrieveUserDataFromStorage();
            }
        }

        _retrieveUserDataFromStorage() {
            const SAVED_USER_LIST = window.localStorage.getItem('users_list');
            if (SAVED_USER_LIST) {
                this._generateUserList(JSON.parse(SAVED_USER_LIST));
            } else {
                this.$listGroup.html('<p id="list-not-available-message">There was an error loading the user list and a saved users list was not found.<br>Please <strong>connect to the internet</strong> to update the users list.</p>');
            }
        }

        _fetchUser() {

            if (this.AJAXrequest) {
                this.AJAXrequest.abort();
                this.AJAXrequest = null;
            }

            this.listUpdateModal.show();

            this.AJAXrequest = $.ajax({
                dataType: "json",
                type: "GET",
                url: "https://promo.pirelli.com/auto/en-ww/geneva-2018/json-report",
                success: data => {
                    this._generateUserList(data);
                    window.localStorage.setItem('users_list', JSON.stringify(data));
                    this.listUpdateModal.hide();
                },
                error: err => {
                    alert("There was an error loading the user list.");
                    this._retrieveUserDataFromStorage();
                    this.listUpdateModal.hide();
                }
            });
        }

        _generateUserList(data) {

            const SORTED_DATA = data.sort(function (a, b) {
                if (a.last_name.toLowerCase() < b.last_name.toLowerCase()) return -1;
                if (a.last_name.toLowerCase() > b.last_name.toLowerCase()) return 1;
                return 0;
            });

            if (this.$usersListItems) {
                this.$usersListItems.off();
            }

            this.$listGroup.empty();

            for (var i = 0; i < SORTED_DATA.length; i++) {
                this.$listGroup.append(this._getUserLineTemplate(SORTED_DATA[i]));
            }

            this.$usersListItems = this.$listGroup.find(".list-group-item");

            this.$usersListItems.on("click", event => {
                this.registeredUserModal.show($(event.delegateTarget).data());
            });
        }

        _getUserLineTemplate(data) {

            const STORAGE_DATA = Globals.getStorageData();
            const HAS_DONE = STORAGE_DATA && STORAGE_DATA[data.user_id] ? " experience-done" : "";
            const DATA_HAD_DONE = STORAGE_DATA && STORAGE_DATA[data.user_id] ? true : false;

            return `<div class="list-group-item list-group-item-action${HAS_DONE}"
                data-experience-done="${DATA_HAD_DONE}"
                data-vehicle_brand="${data.vehicle_brand}"
                data-email="${data.email}"
                data-user_id="${data.user_id}"
                data-first_name="${data.first_name}"
                data-last_name="${data.last_name}">
                    <span></span>
                    <h3>${data.last_name} ${data.first_name}</h3>
                    <p>${data.vehicle_brand}</p>
                </div>`;
        }

        show(isAnimated = true) {

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.set(this.$domElement, { opacity: 1 });

            super.show(isAnimated, nestedTL);
        }

        hide(isAnimated = true) {

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.to(this.$domElement, 0.8, { force3D: "auto", opacity: 0, ease: window.Expo.easeInOut });

            super.hide(isAnimated, nestedTL);
        }

        dispose() {
            this.newUserModal.dispose();
            this.registeredUserModal.dispose();
            this.listUpdateModal.dispose();

            this.newUserModal = null;
            this.registeredUserModal = null;
            this.listUpdateModal = null;

            if (this.$usersListItems) {
                this.$usersListItems.off();
                this.$usersListItems = null;
            }

            if (this.AJAXrequest) {
                this.AJAXrequest.abort();
                this.AJAXrequest = null;
            }

            this.$updateUsersBtn.off();
            this.$updateUsersBtn = null;
            this.$listGroup = null;
            this.$sendUsersBtn = null;

            super.dispose();
        }
    }

    window.UsersListView = UsersListView;
})(window);
