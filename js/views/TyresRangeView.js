(function (window) {

    class TyresRangeView extends window.AbstractViewTimeline {

        static get TEMPLATE() {
            const TPL = $("#tpl__tyres-range-view").text();
            const COMPILED = _.template(TPL);
            return COMPILED({
                general: Globals.BRAND_DATA.general,
                data: Globals.BRAND_DATA.view.tyres_range
            });
        }

        constructor(id, options) {
            super(id, options);
            this.$carousel = null;
            this.$carouselMask = this.$domElement.find("#tyres-range-view__carousel-overlay");
            this.$header = this.$domElement.find(".view-header");
            this.$footer = this.$domElement.find(".view-footer");
        }

        init() {
            super.init();

            this.$carousel = this.$domElement.find(".view-content .view-main .carousel").flickity({
                autoPlay: false,
                prevNextButtons: false,
                setGallerySize: false
            });

            if (window.Globals.BRAND_DATA.general.is_prestige) {
                this.$carousel.on('select.flickity', () => {
                    if (this.$carousel.data('flickity').selectedIndex === 0) {
                        TweenMax.to(this.$header.find("h1 span"), 0, { scrambleText: { text: "Summer", speed: 0.8, ease: Linear.easeNone } });
                    } else {
                        TweenMax.to(this.$header.find("h1 span"), 0, { scrambleText: { text: "Winter", speed: 0.8, ease: Linear.easeNone } });
                    }
                });
            }
        }

        show(isAnimated = true) {

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.fromTo(this.$carouselMask, 0.8, { transformOrigin: "right center", scaleX: 1 }, { force3D: "auto", scaleX: 0, ease: window.Circ.easeInOut });
            nestedTL.addLabel("viewContent", "-=0.35");
            nestedTL.fromTo(this.$header, 0.6, { yPercent: -110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Cubic.easeInOut }, "viewContent");
            nestedTL.fromTo(this.$footer, 0.6, { yPercent: 110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Cubic.easeInOut }, "viewContent");
            nestedTL.fromTo(this.$carousel.find(".flickity-page-dots"), 0.7, { y: 25, autoAlpha: 0 }, { force3D: "auto", y: 0, autoAlpha: 1, ease: window.Cubic.easeOut }, "viewContent+=0.2");

            super.show(isAnimated, nestedTL);
        }

        hide(isAnimated = true) {

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.to(this.$footer.find(".btn-next > *:not(.v-line)"), 0.45, { force3D: "auto", y: 5, opacity: 0, ease: window.Expo.easeInOut }, 0);
            nestedTL.to(this.$header, 0.6, { force3D: "auto", yPercent: -110, opacity: 0, ease: window.Expo.easeInOut }, 0);
            nestedTL.to(this.$carousel.find(".flickity-page-dots"), 0.7, { force3D: "auto", y: 25, autoAlpha: 0, ease: window.Cubic.easeOut }, 0);
            nestedTL.to(this.$carouselMask, 0.75, { force3D: "auto", scaleX: 1, ease: window.Cubic.easeInOut }, 0.25);
            nestedTL.set([this.$carousel, this.$carouselMask], { visibility: "hidden" });
            nestedTL.to(this.$footer, 0.5, { force3D: "auto", opacity: 0, ease: window.Circ.easeOut });

            super.hide(isAnimated, nestedTL);
        }

        dispose() {
            if (this.$carousel) {
                this.$carousel.off();
                this.$carousel.flickity('destroy');
                this.$carousel = null;
            }
            this.$header = null;
            this.$footer = null;
            this.$carouselMask = null;

            super.dispose();
        }
    }

    window.TyresRangeView = TyresRangeView;
})(window);
