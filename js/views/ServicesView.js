(function (window) {

    class ServicesView extends window.AbstractViewTimeline {

        static get TEMPLATE() {
            const TPL = $("#tpl__services-view").text();
            const COMPILED = _.template(TPL);
            return COMPILED({
                general: Globals.BRAND_DATA.general
            });
        }

        constructor(id, options) {

            super(id, options);

            this.$cards = this.$domElement.find(".services__service-card");
            this.$switch = this.$domElement.find(".decision-switch");
            this.$service1 = this.$domElement.find("#services-view__service-1");
            this.$service2 = this.$domElement.find("#services-view__service-2");
            this.$service3 = this.$domElement.find("#services-view__service-3");
            this.$header = this.$domElement.find(".view-header");
            this.$footer = this.$domElement.find(".view-footer");
            this._services = null;

            /* */

            this.$btnNext = this.$domElement.find(".btn-next");
            this.$btnNext.on("click", event => {
                if (this._services) {
                    Globals.setUserData("services", this._services);
                    /* Actually writes data in the local storage */
                    Globals.registerStorageUserData();
                }
            });

            this.$switch.each((i, el) => {
                this.addModules(new window.DecisionSwitch(this.id + "__decision-switch-" + i.toString(), {
                    $domElement: $(el),
                    onChange: value => {

                        this._services = this.$domElement.find('.decision-switch[data-val="yes"]').map((i, el) => {
                            return $(el).attr("data-label");
                        }).toArray();

                        const ALL = this.$domElement.find('.decision-switch').filter((i, el) => {
                            return $(el).attr("data-val");
                        });

                        this.$btnNext[ALL.length == this.$switch.length ? "removeClass" : "addClass"]("disabled");
                    }
                }));
            });

            this.$cards.removeClass("flipped");
        }

        init() {

            this.$cards.on("click", event => {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();

                $(event.delegateTarget).toggleClass("flipped");
            });

            super.init();
        }

        show(animated = true) {
            let nestedTL = new window.TimelineMax({ paused: true });
            nestedTL.fromTo(this.$header, 0.8, { yPercent: -110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Expo.easeInOut });
            nestedTL.staggerFromTo([this.$service1, this.$service2, this.$service3], 1, { y: 50, opacity: 0 }, { force3D: "auto", y: 0, opacity: 1, ease: window.Cubic.easeOut }, 0.15, 0);
            nestedTL.staggerFromTo([this.$service1.find(".decision-switch"), this.$service2.find(".decision-switch"), this.$service3.find(".decision-switch")], 0.5, { y: -25, opacity: 0 }, { force3D: "auto", y: 0, opacity: 1, ease: window.Circ.easeOut }, 0.15, 0.4);
            nestedTL.fromTo(this.$footer, 0.85, { yPercent: 110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Cubic.easeOut }, "-=0.5");
            super.show(animated, nestedTL);
        }

        hide(animated = true) {
            this.$cards.removeClass("flipped");

            let nestedTL = new window.TimelineMax({ paused: true });
            nestedTL.to(this.$footer.find(".btn-next > *:not(.v-line)"), 0.45, { force3D: "auto", y: 5, opacity: 0, ease: window.Expo.easeInOut }, 0);
            nestedTL.to(this.$header, 0.65, { force3D: "auto", yPercent: -110, opacity: 0, ease: window.Expo.easeInOut }, 0);
            nestedTL.staggerTo([this.$service3, this.$service2, this.$service1], 0.85, { force3D: "auto", y: 30, opacity: 0, ease: window.Expo.easeInOut }, 0.1, 0);
            nestedTL.to(this.$footer, 0.5, { force3D: "auto", opacity: 0, ease: window.Circ.easeOut });
            super.hide(animated, nestedTL);
        }

        dispose() {
            this.$header = null;
            this.$footer = null;
            this.$btnNext.off("click");
            this.$cards.off("click");
            this.$cards = null;
            this.$service1 = null;
            this.$service2 = null;
            this.$service3 = null;
            super.dispose();
        }
    }

    window.ServicesView = ServicesView;
})(window);
