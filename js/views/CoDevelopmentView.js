(function (window) {

    class CoDevelopmentView extends window.AbstractViewTimeline {

        static get TEMPLATE() {
            const TPL = $("#tpl__codevelopment-view").text();
            const COMPILED = _.template(TPL);
            return COMPILED({
                general: Globals.BRAND_DATA.general,
                data: Globals.BRAND_DATA.view.co_development
            });
        }

        constructor(id, options) {
            super(id, options);
            this.$carousel = null;
            this.$header = this.$domElement.find(".view-header");
            this.$footer = this.$domElement.find(".view-footer");
            this.$slide2Desc = this.$domElement.find("#codevelopment-view__slide2 .carousel-cell-desc");

            if (Globals.BRAND_DATA.general.is_prestige) {
                this.marksTL = null;
                this.$marks = this.$domElement.find("#codevelopment-view__slide2-marks");
            }
        }

        init() {
            super.init();

            this.$carousel = this.$domElement.find('.view-content .view-main .carousel').flickity({
                autoPlay: false,
                prevNextButtons: false,
                setGallerySize: false
            });

            if (Globals.BRAND_DATA.general.is_prestige) {
                window.TweenMax.set(this.$marks.find("#codevelopment-view__slide2-marks-content"), { autoAlpha: 0 });
                this.$marks.find("#codevelopment-view__slide2-marks-btn").on("click", this.showMarks.bind(this));
                this.$marks.find("#codevelopment-view__slide2-marks-grpx .btn.btn-close-overlay").on("click", this.hideMarks.bind(this));
            }
        }

        showMarks() {
            if (this.marksTL) {
                this.marksTL.kill();
                this.marksTL = null;
            }

            this.marksTL = new window.TimelineMax();
            this.marksTL.to(this.$slide2Desc, 0.6, { y: -25, opacity: 0, ease: window.Expo.easeInOut });

            this.marksTL.addLabel("showMarksContent", "-=0.4");
            this.marksTL.set(this.$marks.find("#codevelopment-view__slide2-marks-content"), { autoAlpha: 1 }, "showMarksContent");
            this.marksTL.fromTo(this.$marks.find("#codevelopment-view__slide2-marks-line"), 0.45, { scaleY: 0 }, { scaleY: 1, ease: window.Circ.easeOut }, "showMarksContent");
            this.marksTL.to(this.$marks.find("#codevelopment-view__slide2-marks-btn"), 0.45, { scale: 0, ease: window.Cubic.easeInOut }, "showMarksContent");
            this.marksTL.fromTo(this.$marks.find("#codevelopment-view__slide2-marks-info"), 0.45, { y: -40, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: window.Circ.easeOut }, "showMarksContent+=0.15");
            this.marksTL.set(this.$marks.find("#codevelopment-view__slide2-marks-btn"), { autoAlpha: 0 });
        }

        hideMarks() {
            if (this.marksTL) {
                this.marksTL.kill();
                this.marksTL = null;
            }

            this.marksTL = new window.TimelineMax();
            this.marksTL.set(this.$marks.find("#codevelopment-view__slide2-marks-btn"), { autoAlpha: 1 }, 0);
            this.marksTL.to(this.$marks.find("#codevelopment-view__slide2-marks-line"), 0.5, { scaleY: 0, ease: window.Expo.easeInOut }, 0);
            this.marksTL.to(this.$marks.find("#codevelopment-view__slide2-marks-info"), 0.5, { y: -40, autoAlpha: 0 }, { y: -40, autoAlpha: 0, ease: window.Expo.easeInOut }, 0);
            this.marksTL.to(this.$marks.find("#codevelopment-view__slide2-marks-btn"), 0.55, { scale: 1, ease: window.Back.easeOut }, 0.2);
            this.marksTL.set(this.$marks.find("#codevelopment-view__slide2-marks-content"), { autoAlpha: 0 });
            this.marksTL.to(this.$slide2Desc, 0.55, { y: 0, opacity: 1, ease: window.Cubic.easeOut });
        }

        show(isAnimated = true) {
            let $activeSlide = this.$carousel.find(".carousel-cell.is-selected");

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.fromTo($activeSlide.find("> img"), 0.65, { opacity: 0, xPercent: 50 }, { force3D: "auto", xPercent: 0, ease: window.Circ.easeOut }, 0);
            nestedTL.to($activeSlide.find("> img"), 1, { force3D: "auto", opacity: 1, ease: window.Circ.easeInOut }, 0);
            nestedTL.addLabel("step1");
            nestedTL.fromTo($activeSlide.find("> .carousel-cell-desc h1"), 0.7, { y: -30, opacity: 0 }, { force3D: "auto", y: 0, opacity: 1, ease: window.Cubic.easeOut }, "step1-=0.3");
            nestedTL.fromTo($activeSlide.find("> .carousel-cell-desc h2"), 0.7, { x: 30, opacity: 0 }, { force3D: "auto", x: 0, opacity: 1, ease: window.Cubic.easeOut }, "step1-=0.15");
            nestedTL.fromTo(this.$header, 0.6, { yPercent: -110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Cubic.easeOut }, "step1+=0.5");
            nestedTL.fromTo(this.$carousel.find(".flickity-page-dots"), 0.7, { y: 15, autoAlpha: 0 }, { force3D: "auto", y: 0, autoAlpha: 1, ease: window.Cubic.easeOut }, "step1+=0.5");
            nestedTL.staggerFromTo($activeSlide.find("> .carousel-cell-desc p, > .carousel-cell-desc ul li"), 0.7, { x: 50, opacity: 0 }, { force3D: "auto", x: 0, opacity: 1, ease: window.Cubic.easeOut }, 0.07, "step1-=0.15");
            nestedTL.staggerFromTo($activeSlide.find("> .carousel-cell-desc #codevelopment-view__timeline .codevelopment-view__timeline-step"), 0.62, { transformOrigin: "center 25px", x: -22, opacity: 0 }, { force3D: "auto", x: 0, opacity: 1, ease: window.Power2.easeOut }, 0.12, "step1");
            nestedTL.staggerFromTo($activeSlide.find("> .carousel-cell-desc #codevelopment-view__timeline .codevelopment-view__timeline-step > span"), 0.62, { transformOrigin: "center 25px", y: -22, opacity: 0 }, { force3D: "auto", y: 0, opacity: 1, ease: window.Power2.easeOut }, 0.12, "step1+=0.15");
            nestedTL.fromTo(this.$footer, 0.85, { yPercent: 110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Cubic.easeOut }, "-=0.5");

            super.show(isAnimated, nestedTL);
        }

        hide(isAnimated = true) {
            if (Globals.BRAND_DATA.general.is_prestige && this.marksTL) {
                this.marksTL.kill();
                this.marksTL = null;
            }

            let $activeSlide = this.$carousel.find(".carousel-cell.is-selected");

            let slideImgFadeXPercent;

            switch (this.$carousel.find(".carousel-cell.is-selected").attr("id")) {
                case "codevelopment-view__slide1":
                    slideImgFadeXPercent = 50;
                    break;

                case "codevelopment-view__slide2":
                    slideImgFadeXPercent = -50;
                    break;
            }

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.to(this.$footer, 0.65, { force3D: "auto", yPercent: 110, opacity: 0, ease: window.Expo.easeInOut });
            nestedTL.to(this.$header, 0.65, { force3D: "auto", yPercent: -110, opacity: 0, ease: window.Expo.easeInOut }, 0);
            nestedTL.to(this.$carousel.find(".flickity-page-dots"), 0.7, { force3D: "auto", y: 15, autoAlpha: 0, ease: window.Expo.easeInOut }, 0);
            nestedTL.to($activeSlide.find("> img"), 1, { force3D: "auto", opacity: 0, ease: window.Circ.easeOut }, 0);
            nestedTL.to($activeSlide.find("> img"), 1, { force3D: "auto", xPercent: slideImgFadeXPercent, ease: window.Circ.easeInOut }, 0);
            nestedTL.to($activeSlide.find("> .carousel-cell-desc"), 0.7, { force3D: "auto", y: 25, opacity: 0, ease: window.Expo.easeInOut }, 0);

            if (Globals.BRAND_DATA.general.is_prestige) {
                nestedTL.to(this.$marks, 0.7, { force3D: "auto", opacity: 0, ease: window.Expo.easeInOut }, 0);
            }

            super.hide(isAnimated, nestedTL);
        }

        dispose() {
            if (this.$carousel) {
                this.$carousel.flickity('destroy');
                this.$carousel = null;
            }

            if (Globals.BRAND_DATA.general.is_prestige) {
                this.$marks.find("#codevelopment-view__slide2-marks-btn").off("click");
                this.$marks.find("#codevelopment-view__slide2-marks-grpx .btn.btn-close-overlay").off("click");
                this.$marks = null;
            }

            this.$slide2Desc = null;
            this.$header = null;
            this.$footer = null;

            super.dispose();
        }
    }

    window.CoDevelopmentView = CoDevelopmentView;
})(window);
