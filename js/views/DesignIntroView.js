(function (window) {

    class DesignIntroView extends window.AbstractViewTimeline {

        static get TEMPLATE() {
            const TPL = $("#tpl__design-intro-view").text();
            const COMPILED = _.template(TPL);
            return COMPILED({
                general: Globals.BRAND_DATA.general,
                data: Globals.BRAND_DATA.view.design_intro
            });
        }

        constructor(id, options) {
            super(id, options);

            this.$header = this.$domElement.find(".view-header");
            this.$footer = this.$domElement.find(".view-footer");
            this.$bgImg = this.$domElement.find(".view-bg .view-bg-img");
        }

        init() {
            super.init();
        }

        show(animated = true) {
            let nestedTL = new window.TimelineMax({ paused: true });

            nestedTL.addLabel("step0", "+=0.5");

            nestedTL.fromTo(this.$header, 1.5, { y: $(window).height() * 2 }, { force3D: "auto", y: 0, ease: window.Expo.easeInOut }, "step0");
            nestedTL.fromTo(this.$header.find("h1"), 1.6, { transformOrigin: "center top", scale: 4, opacity: 0 }, { force3D: "auto", scale: 1, opacity: 1, ease: window.Circ.easeOut }, "step0");
            nestedTL.fromTo(this.$bgImg, 1, { transformOrigin: "left center", opacity: 0, xPercent: 110 }, { opacity: 1, rotation: 0, scale: 1, yPercent: 0, xPercent: 0, ease: window.Power2.easeOut }, "-=0.4");
            nestedTL.fromTo(this.$footer, 0.85, { yPercent: 110, opacity: 0 }, { force3D: "auto", yPercent: 0, opacity: 1, ease: window.Cubic.easeOut });
            super.show(animated, nestedTL);
        }

        hide(animated = true) {
            let nestedTL = new window.TimelineMax({ paused: true });

            nestedTL.to(this.$footer, 0.65, { force3D: "auto", yPercent: 110, opacity: 0, ease: window.Expo.easeInOut });

            if (!Globals.BRAND_DATA.general.is_prestige) {
                nestedTL.to(this.$header, 0.65, { force3D: "auto", yPercent: -110, opacity: 0, ease: window.Expo.easeInOut }, 0);
            }

            nestedTL.to(this.$bgImg, 1, { opacity: 0, xPercent: -110, ease: window.Back.easeIn }, "-=0.3");
            super.hide(animated, nestedTL);
        }

        dispose() {
            this.$header = null;
            this.$footer = null;
            this.$bgImg = null;
            super.dispose();
        }

    }

    window.DesignIntroView = DesignIntroView;
})(window);
