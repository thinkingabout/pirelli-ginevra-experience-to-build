(function (window) {

    class PzeroView extends window.AbstractViewTimeline {

        static get TEMPLATE() {
            const TPL = $("#tpl__pzero-view").text();
            const COMPILED = _.template(TPL);
            return COMPILED({
                general: Globals.BRAND_DATA.general
            });
        }

        constructor(id, options) {

            super(id, options);

            this.$bg = this.$domElement.find(".view-bg");
            this.$flashImgs = this.$domElement.find("#pzero-view-bg-overlay__flashes-container > img");
            this.$wheel = this.$domElement.find("#pzero-view-bg-overlay__wheel");
            this.$text = this.$domElement.find("#pzero-view__info-text");
            this.$header = this.$domElement.find(".view-header");
            this.$footer = this.$domElement.find(".view-footer");

            this.flashTlArray = null;
        }

        init() {

            window.TweenMax.set(this.$flashImgs, {
                opacity: 0
            });

            super.init();

            /*this._startGodRaysFX();*/
        }

        show(isAnimated = true) {

            this.flashTlArray = [];

            this.$flashImgs.each((i, el) => {

                let tl = new window.TimelineMax({
                    delay: window.Math.random() * 10,
                    repeat: -1,
                    repeatDelay: 2 + window.Math.random() * 5
                });

                tl.fromTo(el, 0.3, {
                    opacity: 0
                }, {
                    force3D: "auto",
                    opacity: 1,
                    ease: window.Expo.easeIn
                });

                tl.to(el, 0.3, {
                    force3D: "auto",
                    opacity: 0,
                    ease: window.Expo.easeOut
                }, "+=0.1");

                this.flashTlArray.push(tl);
            });

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.set(this.$wheel, {
                opacity: 1
            });

            nestedTL.fromTo(this.$header, 0.8, {
                yPercent: -110,
                opacity: 0
            }, {
                force3D: "auto",
                yPercent: 0,
                opacity: 1,
                ease: window.Expo.easeInOut
            });

            nestedTL.fromTo(this.$bg, 1.5, {
                opacity: 0
            }, {
                force3D: "auto",
                opacity: 1,
                ease: window.Cubic.easeOut
            }, "-=0.5");

            nestedTL.addLabel("step1", "-=0.2");

            nestedTL.fromTo(this.$wheel, 0.65, {
                yPercent: -15,
                xPercent: 90
            }, {
                force3D: "auto",
                yPercent: 0,
                xPercent: 0,
                ease: window.Cubic.easeOut
            }, "step1");

            nestedTL.fromTo(this.$text, 0.85, {
                y: 50,
                opacity: 0
            }, {
                force3D: "auto",
                y: 0,
                opacity: 1,
                ease: window.Cubic.easeOut
            }, "step1+=0.2");

            nestedTL.fromTo(this.$footer, 0.85, {
                yPercent: 110,
                opacity: 0
            }, {
                force3D: "auto",
                yPercent: 0,
                opacity: 1,
                ease: window.Cubic.easeOut
            }, "+=0.25");

            super.show(isAnimated, nestedTL);
        }

        hide(animated = true) {

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.to(this.$footer, 0.65, { force3D: "auto", yPercent: 110, opacity: 0, ease: window.Expo.easeInOut });

            nestedTL.to(this.$header, 0.65, {
                force3D: "auto",
                yPercent: -110,
                opacity: 0,
                ease: window.Expo.easeInOut
            }, 0);

            nestedTL.to(this.$text, 0.85, {
                force3D: "auto",
                y: 50,
                opacity: 0,
                ease: window.Expo.easeInOut
            }, 0);

            nestedTL.addLabel("bg", "-=0.2");

            nestedTL.to(this.$wheel, 0.55, {
                force3D: "auto",
                yPercent: 2,
                xPercent: -12,
                opacity: 0,
                ease: window.Cubic.easeInOut
            }, "bg");

            nestedTL.to(this.$bg, 0.85, {
                force3D: "auto",
                opacity: 0,
                ease: window.Expo.easeInOut
            }, "bg");

            super.hide(animated, nestedTL);
        }

        dispose() {

            window.cancelAnimationFrame(this.animationID);

            if (this.renderer) {
                this.renderer.destroy(true); // true = removes the view
                this.renderer = null;
            }

            if (this.stage) {
                this.stage.destroy(true); // true = destroys children
                this.stage = null;
            }

            for (let i = 0, len = this.flashTlArray.length; i < len; i++) {
                this.flashTlArray[i].kill();
            }

            this.flashTlArray = null;
            this.$header = null;
            this.$text = null;
            this.$bg = null;
            this.$wheel = null;
            this.$flasImgs = null;
            this.$footer = null;

            super.dispose();
        }

        // -------------------------------- GOD RAYS -----------------------------------------

        /*_startGodRaysFX() {
             let $bgImg = this.$domElement.find(".view-bg-img");
             this.renderer = PIXI.autoDetectRenderer(2732, 2048, {
                transparent: true,
                view: this.$domElement.find("canvas")[0]
            });
             this.stage = new PIXI.Container();
             this.texture = PIXI.Texture.fromImage(
                $bgImg.css("background-image").slice(4, -1).replace(/"/g, "")
            );
             $bgImg.css("background-image", "none");
             this.sprite = new PIXI.Sprite(this.texture);
             this.stage.addChild(this.sprite);
             this.sprite.filters = [
                new PIXI.filters.GodrayFilter({
                    angle: 35, // number 30 optional. Angle/Light-source of the rays.
                    gain: 0.4, // number 0.5 optional. General intensity of the effect.
                    lacunrity: 2.3, // number 0.5 optional. The density of the fractal noise.
                    parallel: true, // boolean true optional. true to use angle, false to use center
                    time: 0, // number 0 optional. The current time position.
                    center: [100, -100] // PIXI.Point | Array.<number> [0 0] optional. Focal point for non-parallel rays, to use this parallel must be set to false.
                })
            ];
             var _self = this;
             var SPEED = 0.0005;
             function animate() {
                _self.sprite.filters[0].time = performance.now() * SPEED;
                _self.animationID = requestAnimationFrame(animate);
                _self.renderer.render(_self.stage);
            }
             animate();
        }*/

        // -------------------------------------------------------------------------
    }

    window.PzeroView = PzeroView;
})(window);
