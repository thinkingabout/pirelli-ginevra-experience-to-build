(function (window) {

    class ThankyouView extends window.AbstractViewTimeline {

        static get TEMPLATE() {
            const TPL = $("#tpl__thankyou-view").text();
            const COMPILED = _.template(TPL);
            return COMPILED({
                general: Globals.BRAND_DATA.general,
                username: Globals.currentUser.first_name + " " + Globals.currentUser.last_name
            });
        }

        constructor(id, options) {
            super(id, options);

            this.$header = this.$domElement.find(".view-header");
            this.$footer = this.$domElement.find(".view-footer");
            this.$productsRow = this.$domElement.find("#thankyou-view__products-row");
            this.$productPzeroColorCol = this.$productsRow.find("#thankyou-view__pzero-color-col");
            this.$productPlus1 = $(this.$productsRow.find(".thankyou-view__survey-plus")[0]);
            this.$productConnessoCol = this.$productsRow.find("#thankyou-view__connesso-col");
            this.$productPlus2 = $(this.$productsRow.find(".thankyou-view__survey-plus")[1]);
            this.$productPzeroTrofeoCol = this.$productsRow.find("#thankyou-view__pzero-trofeo-col");

            this.$servicesRow = this.$domElement.find("#thankyou-view__services-row");
            this.$servicePremiumServiceCol = this.$servicesRow.find("#thankyou-view__premium-service-col");
            this.$serviceHomeServiceCol = this.$servicesRow.find("#thankyou-view__home-service-col");
            this.$serviceSmartServiceCol = this.$servicesRow.find("#thankyou-view__smart-service-col");

            this.noChoiceRow = this.$domElement.find("#thankyou-view__no-choice-row");

            // this.$domElement.find("#thankyou-view__username").text(Globals.currentUser.first_name + " " + Globals.currentUser.last_name);

            this.$pirelli = this.$footer.find("img");

            this.$pirelli.on("touchstart", event => {
                this._timeout = window.setTimeout(() => {
                    if (confirm("Do you want to restart the application?")) {
                        document.location.href = 'index.html';
                    }
                }, 3000);
            });

            this.$pirelli.on("touchend", event => {
                window.clearTimeout(this._timeout);
            });
        }

        init() {
            this.$productsRow.removeClass("is-selected");
            this.$productPzeroColorCol.removeClass("is-selected");
            this.$productPlus1.removeClass("is-selected");
            this.$productConnessoCol.removeClass("is-selected");
            this.$productPlus2.removeClass("is-selected");
            this.$productPzeroTrofeoCol.removeClass("is-selected");

            this.$servicesRow.removeClass("is-selected");
            this.$servicePremiumServiceCol.removeClass("is-selected");
            this.$serviceHomeServiceCol.removeClass("is-selected");
            this.$serviceSmartServiceCol.removeClass("is-selected");

            this.noChoiceRow.removeClass("is-selected");

            let _survey = Globals.getUserData("survey");
            let _services = Globals.getUserData("services");

            // SURVEY

            if (!_survey) {
                console.warn("No survey selected!");
                _survey = [];
            }

            if (!_services) {
                console.warn("No services selected!");
                _services = [];
            }

            _survey = _survey.join();
            _services = _services.join();

            if (!_survey.length && !_services.length) {

                this.noChoiceRow.addClass("is-selected");
            } else {

                if (_survey.length) {

                    this.$productsRow.addClass("is-selected");

                    const SELECTED_COLOR = Globals.getUserData("color");

                    if (_survey.match("customize-colors")) {
                        this.$productPzeroColorCol.addClass("is-selected");
                        if (SELECTED_COLOR) {
                            this.$productPzeroColorCol.find(".thankyou-view__product-wheel").css("border-color", SELECTED_COLOR.hex);
                            this.$productPzeroColorCol.find(".card-title > span").text(SELECTED_COLOR.data);
                        }
                    }

                    if (_survey.match("new-technologies")) {
                        this.$productConnessoCol.addClass("is-selected");

                        if (SELECTED_COLOR) {
                            this.$productConnessoCol.find("#thankyou-view__connesso-wheel-default").remove();
                            this.$productConnessoCol.find("#thankyou-view__connesso-wheel").addClass("customize-colors-wheel").css("border-color", SELECTED_COLOR.hex);
                        } else {
                            this.$productConnessoCol.find("#thankyou-view__connesso-wheel-cutomizable").remove();
                        }
                    }

                    if (_survey.match("drive-competitions")) {
                        this.$productPzeroTrofeoCol.addClass("is-selected");
                    }

                    if (_survey == "customize-colors,new-technologies") {
                        this.$productPlus1.addClass("is-selected");
                    } else if (_survey == "customize-colors,drive-competitions") {
                        this.$productPlus1.addClass("is-selected");
                    } else if (_survey == "new-technologies,drive-competitions") {
                        this.$productPlus2.addClass("is-selected");
                    } else if (_survey == "customize-colors,new-technologies,drive-competitions") {
                        this.$productPlus1.addClass("is-selected");
                        this.$productPlus2.addClass("is-selected");
                    }
                }

                if (_services.length) {

                    this.$servicesRow.addClass("is-selected");

                    if (_services.match("premium-service")) {
                        this.$servicePremiumServiceCol.addClass("is-selected");
                    }

                    if (_services.match("home-service")) {
                        this.$serviceHomeServiceCol.addClass("is-selected");
                    }

                    if (_services.match("smart-service")) {
                        this.$serviceSmartServiceCol.addClass("is-selected");
                    }
                }
            }

            super.init();
        }

        show(isAnimated = true) {

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.addLabel("step0", "+=0.4");
            nestedTL.fromTo(this.$header, 1.5, { y: $(window).height() * 2 }, { force3D: "auto", y: 0, ease: window.Expo.easeInOut }, "step0");
            nestedTL.fromTo(this.$header.find("h2"), 1.6, { transformOrigin: "center top", scale: 4, opacity: 0 }, { force3D: "auto", scale: 1, opacity: 1, ease: window.Circ.easeOut }, "step0");
            nestedTL.addLabel("step1", "-=0.3");

            if (this.noChoiceRow.hasClass("is-selected")) {

                nestedTL.fromTo(this.noChoiceRow, 0.5, {
                    y: 25,
                    opacity: 0
                }, {
                    force3D: "auto",
                    y: 0,
                    opacity: 1,
                    ease: window.Cubic.easeOut
                }, "step1");
            } else {

                if (this.$productsRow.hasClass("is-selected")) {
                    let productArray = [];
                    let plusArray = [];

                    if (this.$productPzeroColorCol.hasClass("is-selected")) productArray.push(this.$productPzeroColorCol);
                    if (this.$productPlus1.hasClass("is-selected")) plusArray.push(this.$productPlus1);
                    if (this.$productConnessoCol.hasClass("is-selected")) productArray.push(this.$productConnessoCol);
                    if (this.$productPlus2.hasClass("is-selected")) plusArray.push(this.$productPlus2);
                    if (this.$productPzeroTrofeoCol.hasClass("is-selected")) productArray.push(this.$productPzeroTrofeoCol);

                    nestedTL.fromTo(this.$productsRow.find(".thankyou-view__main-view-title"), 0.5, {
                        y: -25,
                        opacity: 0
                    }, {
                        force3D: "auto",
                        y: 0,
                        opacity: 1,
                        ease: window.Cubic.easeOut
                    }, "step1");
                    nestedTL.staggerFromTo(productArray, 0.5, {
                        y: 25,
                        opacity: 0
                    }, {
                        force3D: "auto",
                        y: 0,
                        opacity: 1,
                        ease: window.Cubic.easeOut
                    }, 0.15, "step1+=0.25");
                    nestedTL.staggerFromTo(plusArray, 0.5, {
                        transformOrigin: "center",
                        scale: 0
                    }, {
                        force3D: "auto",
                        scale: 1,
                        opacity: 1,
                        ease: window.Cubic.easeOut
                    }, 0.15, "step1+=0.35");
                }

                if (this.$servicesRow.hasClass("is-selected")) {

                    let servicesArray = [];

                    if (this.$servicePremiumServiceCol.hasClass("is-selected")) servicesArray.push(this.$servicePremiumServiceCol);
                    if (this.$serviceHomeServiceCol.hasClass("is-selected")) servicesArray.push(this.$serviceHomeServiceCol);
                    if (this.$serviceSmartServiceCol.hasClass("is-selected")) servicesArray.push(this.$serviceSmartServiceCol);

                    nestedTL.fromTo(this.$servicesRow.find(".thankyou-view__main-view-title"), 0.5, {
                        y: -25,
                        opacity: 0
                    }, {
                        force3D: "auto",
                        y: 0,
                        opacity: 1,
                        ease: window.Cubic.easeOut
                    }, "step1+=0.25");
                    nestedTL.staggerFromTo(servicesArray, 0.5, {
                        y: 25,
                        opacity: 0
                    }, {
                        force3D: "auto",
                        y: 0,
                        opacity: 1,
                        ease: window.Cubic.easeOut
                    }, 0.15, "step1+=0.5");
                }
            }

            nestedTL.fromTo(this.$footer, 0.8, {
                yPercent: 110,
                opacity: 0
            }, {
                force3D: "auto",
                yPercent: 0,
                opacity: 1,
                ease: window.Cubic.easeOut
            }, "-=0.5");

            super.show(isAnimated, nestedTL);
        }

        onShowStart() {

            window.particlesJS("thankyou-view__connesso-slow-particles-wrapper", {
                "particles": {
                    "number": {
                        "value": 30,
                        "density": {
                            "enable": true,
                            "value_area": 900
                        }
                    },
                    "color": {
                        "value": "#249494"
                    },
                    "shape": {
                        "type": "circle",
                        "stroke": {
                            "width": 0,
                            "color": "#249494"
                        },
                        "polygon": {
                            "nb_sides": 5
                        },
                        "image": {
                            "src": "img/github.svg",
                            "width": 100,
                            "height": 100
                        }
                    },
                    "opacity": {
                        "value": 0.5,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 1,
                            "opacity_min": 0.1,
                            "sync": false
                        }
                    },
                    "size": {
                        "value": 12,
                        "random": false,
                        "anim": {
                            "enable": false,
                            "speed": 40,
                            "size_min": 0.1,
                            "sync": false
                        }
                    },
                    "line_linked": {
                        "enable": true,
                        "distance": 150,
                        "color": "#6cfcfc",
                        "opacity": 0.4,
                        "width": 4
                    },
                    "move": {
                        "enable": true,
                        "speed": 1.2,
                        "direction": "top-right",
                        "random": false,
                        "straight": false,
                        "out_mode": "out",
                        "attract": {
                            "enable": false,
                            "rotateX": 600,
                            "rotateY": 1200
                        }
                    }
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                        "onhover": {
                            "enable": false,
                            "mode": "repulse"
                        },
                        "onclick": {
                            "enable": true,
                            "mode": "push"
                        },
                        "resize": true
                    },
                    "modes": {
                        "grab": {
                            "distance": 400,
                            "line_linked": {
                                "opacity": 1
                            }
                        },
                        "bubble": {
                            "distance": 400,
                            "size": 40,
                            "duration": 2,
                            "opacity": 8,
                            "speed": 3
                        },
                        "repulse": {
                            "distance": 200
                        },
                        "push": {
                            "particles_nb": 4
                        },
                        "remove": {
                            "particles_nb": 2
                        }
                    }
                },
                "retina_detect": true,
                "config_demo": {
                    "hide_card": false,
                    "background_color": "#b61924",
                    "background_image": "",
                    "background_position": "50% 50%",
                    "background_repeat": "no-repeat",
                    "background_size": "cover"
                }
            });

            window.particlesJS("thankyou-view__connesso-fast-particles-wrapper", {
                "particles": {
                    "number": {
                        "value": 40,
                        "density": {
                            "enable": true,
                            "value_area": 350
                        }
                    },
                    "color": {
                        "value": "#37e0e0"
                    },
                    "shape": {
                        "type": "circle",
                        "stroke": {
                            "width": 0,
                            "color": "#37e0e0"
                        },
                        "polygon": {
                            "nb_sides": 5
                        },
                        "image": {
                            "src": "img/github.svg",
                            "width": 100,
                            "height": 100
                        }
                    },
                    "opacity": {
                        "value": 0.7,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 1,
                            "opacity_min": 0.1,
                            "sync": false
                        }
                    },
                    "size": {
                        "value": 3,
                        "random": false,
                        "anim": {
                            "enable": false,
                            "speed": 40,
                            "size_min": 0.1,
                            "sync": false
                        }
                    },
                    "line_linked": {
                        "enable": true,
                        "distance": 130,
                        "color": "#37e0e0",
                        "opacity": 0.3,
                        "width": 2
                    },
                    "move": {
                        "enable": true,
                        "speed": 8,
                        "direction": "top-right",
                        "random": false,
                        "straight": false,
                        "out_mode": "out",
                        "attract": {
                            "enable": false,
                            "rotateX": 600,
                            "rotateY": 1200
                        }
                    }
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                        "onhover": {
                            "enable": false,
                            "mode": "repulse"
                        },
                        "onclick": {
                            "enable": false,
                            "mode": "push"
                        },
                        "resize": true
                    },
                    "modes": {
                        "grab": {
                            "distance": 400,
                            "line_linked": {
                                "opacity": 1
                            }
                        },
                        "bubble": {
                            "distance": 400,
                            "size": 40,
                            "duration": 2,
                            "opacity": 8,
                            "speed": 3
                        },
                        "repulse": {
                            "distance": 200
                        },
                        "push": {
                            "particles_nb": 4
                        },
                        "remove": {
                            "particles_nb": 2
                        }
                    }
                },
                "retina_detect": true,
                "config_demo": {
                    "hide_card": false,
                    "background_color": "#b61924",
                    "background_image": "",
                    "background_position": "50% 50%",
                    "background_repeat": "no-repeat",
                    "background_size": "cover"
                }
            });

            super.onShowStart();
        }

        hide(isAnimated = true) {

            let nestedTL = new window.TimelineMax({
                paused: true
            });

            nestedTL.to(this.$domElement, 0.8, {
                force3D: "auto",
                opacity: 0,
                ease: window.Expo.easeInOut
            });

            super.hide(isAnimated, nestedTL);
        }

        dispose() {

            if (window.pJSDom[0]) {
                window.pJSDom[0].pJS.fn.vendors.destroypJSAtIndex(0);
            }

            this.$header = null;
            this.$footer = null;
            this.$productsRow = null;
            this.$productPzeroColorCol = null;
            this.$productPlus1 = null;
            this.$productConnessoCol = null;
            this.$productPlus2 = null;
            this.$productPzeroTrofeoCol = null;

            this.$servicesRow = null;
            this.$servicePremiumServiceCol = null;
            this.$serviceHomeServiceCol = null;
            this.$serviceSmartServiceCol = null;

            this.noChoiceRow = null;

            super.dispose();
        }
    }

    window.ThankyouView = ThankyouView;
})(window);
