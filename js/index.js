var app = {

    // Application Constructor
    initialize: function () {

        var p1 = new Promise(function (resolve, reject) {
            $(document).ready(function (event) {
                resolve();
            });
        });

        var p2 = new Promise(function (resolve, reject) {
            if (Globals.isRealDeviceApp) {
                document.addEventListener("deviceready", () => {
                    resolve();
                }, false);
            } else {
                resolve();
            }
        });

        Promise.all([p1, p2]).then(() => {
            this.runApp();

            $.validator.addMethod("lettersonly", function (value, element) {
                return this.optional(element) || /^[A-Za-z\s]+$/i.test(value);
            }, "Only alphabetical characters");
        });
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.

    runApp: function () {

        console.log('Run the App!');

        if (Globals.isRealDeviceApp && window.plugins && window.plugins.insomnia) {
            window.plugins.insomnia.keepAwake();
        }

        if (Globals.isRealDeviceApp && window.StatusBar) {
            window.StatusBar.hide();
        }

        /**
         *  Wheel
         */

        let wheel = new window.PirelliWheel("pirelli-wheel", {
            $domElement: $("#pirelli-wheel-container"),
            watchResize: false
        });

        wheel.init();

        /**
         *  Views setup
         */

        let SECTION_WITH_WHEEL = [];

        let TM_CONFIGS = {

            views: {
                home: {
                    path: Globals.HOME,
                    view: {
                        class: window.UsersListView,
                        options: {}
                    }
                },
                userslist: {
                    path: Globals.USERS_LIST_VIEW,
                    view: {
                        class: window.UsersListView,
                        options: {}
                    }
                },
                welcome: {
                    path: Globals.WELCOME_VIEW,
                    view: {
                        class: window.WelcomeView,
                        options: {}
                    }
                },
                design_intro: {
                    path: Globals.DESIGN_INTRO_VIEW,
                    view: {
                        class: window.DesignIntroView,
                        options: {}
                    }
                },
                design: {
                    path: Globals.DESIGN_VIEW,
                    view: {
                        class: window.DesignView,
                        options: {}
                    }
                },
                codevelopment: {
                    path: Globals.CODEVELOPMENT_VIEW,
                    view: {
                        class: window.CoDevelopmentView,
                        options: {}
                    }
                },
                tyres_range: {
                    path: Globals.TYRES_RANGE_VIEW,
                    view: {
                        class: window.TyresRangeView,
                        options: {}
                    }
                },
                technology: {
                    path: Globals.TECHNOLOGY_VIEW,
                    view: {
                        class: window.TechnologyView,
                        options: {
                            wheel: wheel
                        }
                    }
                },
                color: {
                    path: Globals.COLOR_VIEW,
                    view: {
                        class: window.ColorView,
                        options: {
                            wheel: wheel
                        }
                    }
                },
                connesso: {
                    path: Globals.CONNESSO_VIEW,
                    view: {
                        class: window.ConnessoView,
                        options: {}
                    }
                },
                pzero: {
                    path: Globals.PZERO_VIEW,
                    view: {
                        class: window.PzeroView,
                        options: {}
                    }
                },
                survey: {
                    path: Globals.SURVEY_SETUP_VIEW,
                    view: {
                        class: window.SurveySetupView,
                        options: {
                            wheel: wheel
                        }
                    }
                },
                service: {
                    path: Globals.SERVICES_VIEW,
                    view: {
                        class: window.ServicesView,
                        options: {}
                    }
                },
                thankyou: {
                    path: Globals.THANKYOU_VIEW,
                    view: {
                        class: window.ThankyouView,
                        options: {}
                    }
                },
                default: {
                    notfound: Globals.WELCOME_VIEW,
                    options: {
                        appendTo: "#section-wrapper",
                        disposeWhenHidden: true,
                        watchResize: false
                    }
                }
            },
            transitions: {
                welcome: {
                    design_intro: TransitionManager.HIDE_AND_SHOW,
                    default: TransitionManager.HIDE_THEN_SHOW
                },
                service: {
                    thankyou: TransitionManager.HIDE_AND_SHOW,
                    default: TransitionManager.HIDE_THEN_SHOW
                },
                tyres_range: {
                    technology: TransitionManager.HIDE_AND_SHOW,
                    default: TransitionManager.HIDE_THEN_SHOW
                },
                default: {
                    default: TransitionManager.HIDE_THEN_SHOW
                }
            }
        };

        _.each(TM_CONFIGS.views, (el, i) => {
            if (el.view && el.view.options && el.view.options.wheel) {
                SECTION_WITH_WHEEL.push(i);
            }
        });

        SECTION_WITH_WHEEL = SECTION_WITH_WHEEL.join("|");

        TransitionManager.init(TM_CONFIGS);

        HashRouter.init({
            onInit: HashRouter_onInit,
            onChange: HashRouter_onUpdate
        });

        function HashRouter_onInit(data) {

            let tr = TransitionManager.change(data.newURL[0], null, true);

            onViewInit(tr.toView);

            console.log("----->TRANSITION_MANAGER: ", tr, data, data.changeType == HashRouter.INTERNAL_CHANGE);
        }

        function HashRouter_onUpdate(data) {

            let tr = TransitionManager.change(data.newURL[0] || "", data.oldURL[0] || "", true // animated
            );

            onViewInit(tr.toView);

            console.log("-----> TRANSITION_MANAGER: ", tr);
        }

        /* Additional settings on view init */

        function onViewInit(currentView) {

            if (currentView.showStatus) {
                onViewShowStart.bind(currentView)();
            } else {
                currentView.$domElement.one(AbstractElement.EL_SHOW_START, onViewShowStart.bind(currentView));
            }

            if (currentView.hideStatus) {
                onViewHideComplete.bind(currentView)();
            } else {
                currentView.$domElement.one(AbstractElement.EL_HIDE_COMPLETE, onViewHideComplete.bind(currentView));
            }

            if (currentView instanceof ColorView) {
                currentView.onCustomColorSelect = function (color, data) {
                    wheel.setCustomColor(color, data);
                };
            }
        }

        function onViewShowStart(event) {
            if (this instanceof WelcomeView) {
                wheel.mode = PirelliWheel.WELCOME_MODE;
            } else if (this instanceof SurveySetupView) {
                wheel.mode = PirelliWheel.SURVEY_SETUP_MODE;
            } else if (this instanceof TechnologyView) {
                wheel.mode = PirelliWheel.TECHNOLOGY_MODE;
            } else if (this instanceof ColorView) {
                wheel.mode = PirelliWheel.COLORS_MODE;
            } else {
                wheel.mode = null;
            }
        }

        function onViewHideComplete(event) {

            // if the current section has the wheel too do nothing
            if (HashRouter.getURL().match(SECTION_WITH_WHEEL)) {
                return;
            }

            wheel.mode = null;
        }
    }
};

app.initialize();
